#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 06:53:03 2020

@author: hp
"""

import sys, PyPDF2

def main():
	# ejemplo de uso: python extraer_paginas_pdf.py origen.pdf 1 20
    origen = PyPDF2.PdfFileReader(sys.argv[1])
    desde , hasta = sys.argv[2:]
    destino = PyPDF2.PdfFileWriter()
    lista = range(int(desde)-1,int(hasta))
    [destino.addPage(origen.getPage(i)) for i in lista]
    destino.write(open('salida.pdf','wb'))
    print('Argument List: {}'.format( str(sys.argv)))

if __name__ == '__main__':
    main()