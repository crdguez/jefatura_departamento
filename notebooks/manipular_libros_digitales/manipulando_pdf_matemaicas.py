#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 09:00:37 2020

@author: hp
"""

import sys, os, PyPDF2

numero = 3

destino = PyPDF2.PdfFileWriter()

os.chdir("dir_destino")

def manipular_1(fichero, destino) :
    
    origen = PyPDF2.PdfFileReader(fichero, strict=False)
    page = origen.getPage(1)
    altura = page.cropBox.getHeight()
    anchura = page.cropBox.getWidth()
    
    for i in range(2,origen.numPages) :
        origen = PyPDF2.PdfFileReader(fichero, strict=False)
        page1 = origen.getPage(i)
        # page1.compressContentStreams()
        page1.cropBox.lowerLeft = (33,altura/2+35)
        page1.cropBox.upperRight = (anchura/2-13,altura-40)
        destino.addPage(page1)
        
        
        origen = PyPDF2.PdfFileReader(fichero, strict=False)
        page2 = origen.getPage(i)
        # page2.compressContentStreams()
        page2.cropBox.lowerLeft = (anchura/2+11,altura/2+35)
        page2.cropBox.upperRight = (anchura-35,altura-40)
        destino.addPage(page2)
    return destino

archivos = os.listdir()
archivos.sort()

[manipular_1(j, destino) for j in archivos]
#manipular_1("02.pdf", destino)

destino.write(open('salida.pdf','wb'))

#destino.write(open('salida.pdf','wb'))