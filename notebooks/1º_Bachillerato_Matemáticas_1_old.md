### 1º Bachillerato Matemáticas 1 
 
#### Unidad 1. Los Números Reales.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &                                                                                            A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &                                                                                            A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &                                                                                            A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &                                                                                            A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &                                                                                                                   1. Sentido de las operaciones.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &                                                                                                                   1. Sentido de las operaciones.\newline  \\
\end{longtable}
 
#### Unidad 2. Álgebra.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &                 A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
\end{longtable}
 
#### Unidad 3. Resolución de triángulos.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                             Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                             Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &                          F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
\end{longtable}
 
#### Unidad 4. Fórmulas y funciones 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                         Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                         Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &                                                      F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline 4. Relaciones y funciones.\newline  \\
\end{longtable}
 
#### Unidad 5. Números Complejos.   
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                         Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                         Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &                                                      F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline contextos.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &             A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline B. Sentido de la medida. \newline 1. Medición.\newline D. Sentido Algebraico.\newline 2. Modelo matemático.\newline 3. Igualdad y desigualdad.\newline  \\
\end{longtable}
 
#### Unidad 6. Vectores. 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                        Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                        Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &     F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
\end{longtable}
 
#### Unidad 7. Geometría analítica.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                                                                  Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                                                                  Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &                                                                                               F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
\end{longtable}
 
#### Unidad 8. Lugares geométricos. Cónicas.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                                                                  Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                                                                                                                  Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. &                                                                                               F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & A. Sentido Numérico. \newline 1. Sentido de las operaciones.\newline 2. Relaciones.\newline C. Sentido espacial\newline 1. Formas geométricas de dos dimensiones.\newline 2. Localización y sistemas de representación.\newline 3. Visualización, razonamiento y modelización geométrica\newline  \\
\end{longtable}
 
#### Unidad 9. Funciones elementales.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                  2.1 Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &                                        D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
\end{longtable}
 
#### Unidad 10. Límites de funciones. 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
\end{longtable}
 
#### Unidad 11. Derivadas.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. & B. Sentido de la medida. \newline 2. Cambio.\newline D. Sentido algebraico.\newline 2. Modelo matemático. \newline 4. Relaciones y funciones.\newline 5. Pensamiento computacional.\newline  \\
\end{longtable}
 
#### Unidad 12. Distribuciones Bidimensionales.  
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                     8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados. &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
                                                                                                                     8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor &                                                                    E. Sentido estocástico.\newline 1. Organización y análisis de datos.\newline 3. Inferencia.\newline  \\
\end{longtable}
 
#### Unidad 13. Combinatoria y probabilidad. 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                       Criterios de evaluación &                                                                                                                                    Saberes básicos \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
6.2. Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad. & F. Sentido socioafectivo.\newline 1. Creencias, actitudes y emociones.\newline 2. Trabajo en equipo y toma de decisiones.\newline 3. Inclusión, respeto y diversidad.\newline  \\
                            1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                            1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                 2.1. Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                         7.1. Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                              7.2. Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                                  5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                5.2. Resolver problemas en contextos matemáticos, estableciendo y aplicando conexiones entre las diferentes ideas matemáticas. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                     8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados. &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
                                                                                                                     8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor &                                                               B. Sentido de la medida.\newline 1. Medición.\newline E. Sentido estocástico.\newline 2. Incertidumbre.\newline  \\
\end{longtable}
 
