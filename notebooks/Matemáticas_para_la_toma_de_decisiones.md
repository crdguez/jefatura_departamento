### Matemáticas para la toma de decisiones 
 
#### Unidad 1. Aritmética modular y criptografía 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                              Saberes básicos &                                                                                                                                                                                                                                                                      Criterios de evaluación \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                              Saberes básicos &                                                                                                                                                                                                                                                                      Criterios de evaluación \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
                                                    A.1. Aritmética en Z\newline - Números primos. \newline - El teorema fundamental de la aritmética.\newline - Ecuaciones diofánticas lineales.  &                                                                                                                                                                      1.2. Resolver ecuaciones diofánticas lineales en una y dos variables, estudiando previamente la existencia de solución. \\
                   A.1. Aritmética en Z:\newline - La relación de divisibilidad. \newline - Máximo común divisor y mínimo común múltiplo\newline - Algoritmo de Euclides. \newline - Identidad de Bezout. &                                                                                                                                                        1.1. Aplicar el algoritmo de Euclides para calcular el m.c.d. de dos números y para obtener la expresión de la identidad de Bezout.\newline  \\
                                                                                                                                                      A.2. Aritmética modular & 1.3. Poseer los fundamentos necesarios para trabajar módulo un entero m, sabiendo las diferentes propiedades\newline que surgen según m sea primo o no. 1.4. Resolver de forma constructiva sistemas de congruencias lineales con una incógnita, estudiando previamente\newline la existencia de solución. \\
                                                                                                                    A.3. El conjunto Z/mZ:\newline - El conjunto de clases módulo m. &                                                                                                                                                                                                             1.5. Conocer y determinar unidades y divisores de cero en Z/mZ para cualquier m. \\
          A.3. El conjunto Z/mZ:\newline - Unidades y divisores de cero. La función phi de Euler.\newline - Orden de un elemento. \newline - El pequeño teorema de Fermat y el teorema de Euler.\newline  &                                                                                                                                                                                                     1.6. Aplicar el pequeño teorema de Fermat para estudiar la primalidad de un entero dado. \\
                                                                                                                                      A.4. Criptografía:\newline - El algoritmo RSA. &                                                                                                                                                                                           1.8. Conocer los fundamentos y vulnerabilidades del algoritmo RSA, aplicándolo en casos sencillos. \\
A.4. Criptografía:\newline - Esteganografía y criptografía. \newline - Origen, utilidad y aplicaciones.\newline - Cifrados de sustitución y polialfabéticos.\newline - Cifrados simétricos y asimétricos. &                                                                                                                                                           1.7. Conocer, idear y aplicar algoritmos de cifrado de sustitución y polialfabéticos sencillos, entendiendo sus\newline vulnerabilidades. \\
\end{longtable}
 
#### Unidad 2. Teoría de grafos 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                                                                                                       Saberes básicos &                                                                                                                                                                                                                                                                                                                                                                           Criterios de evaluación \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                                                                                                       Saberes básicos &                                                                                                                                                                                                                                                                                                                                                                           Criterios de evaluación \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
B.1. Definición, conceptos y propiedades básicas.\newline - Definición intuitiva de grafo. Vértices y aristas.\newline - Representaciones pictóricas. Isomorfismo de grafos.\newline - Grafos dirigidos. Grafos ponderados.\newline - Subgrafos. Ciclos y caminos.\newline - Conexión. Grafos bipartitos.\newline - Planaridad y coloreabilidad. &                                                                                                                                                                                                                                                                                                                                                 2.1. Identificar propiedades y tipos de grafos.\newline  \\
                                                                                                                                   B.2. Tipos y familias de grafos.\newline - Grafo ciclo y grafo camino.\newline - Grafos completos. Grafos bipartitos completos.\newline - Árboles.\newline - Grafos eulerianos y hamiltonianos. &                                           2.2. Clasificar grafos según distintos criterios.\newline  2.3. Formular definiciones de las principales propiedades y familias de grafos haciendo uso de lenguaje especializado.\newline  2.4. Proporcionar argumentos y/o contraejemplos acerca de la existencia, o no, de ciertos tipos de grafos y respecto al cumplimiento, o no, de determinadas propiedades.\newline  \\
                                                                                                                                                                               B.3. Algoritmos de grafos:\newline - El algoritmo voraz de coloración.\newline - El algoritmo de Fleury.\newline - El algoritmo de Dijkstra. & 2.5. Utilizar grafos para modelizar matemáticamente situaciones de la vida cotidiana, la ciencia y la tecnología.\newline  2.6. Proponer situaciones y problemas reales susceptibles de ser modelizados utilizando la teoría de grafos.\newline  2.7. Aplicar adecuadamente algoritmos sencillos sobre grafos, reflexionando sobre su eficiencia y transfiriendo el resultado a la situación real de partida.\newline  \\
\end{longtable}
 
#### Unidad 3. Teoría de juegos 
 
\begin{longtable}{p{8cm} | p{10cm}}
\toprule
                                                                                                                                                                                                            Saberes básicos &                                                                                                                                                                   Criterios de evaluación \\
\midrule
\endfirsthead

\toprule
                                                                                                                                                                                                            Saberes básicos &                                                                                                                                                                   Criterios de evaluación \\
\midrule
\endhead
\midrule
\multicolumn{2}{r}{{Sigue en la siguiente página}} \\
\midrule
\endfoot

\bottomrule
\endlastfoot
                                                     B.1. Definiciones básicas.\newline - Concepto de juego. \newline - Juegos de azar y deterministas. \newline - Información perfecta e imperfecta.\newline - Vector de pagos. Juegos de suma cero.\newline  &                                                                    3.1. Conocer la terminología básica propia de la teoría de juegos y utilizarla adecuadamente en situaciones oportunas. \\
                                                                                                                                                                                        B.2. Formas de representar un juego &                                                 3.3. Comprender los conceptos de estrategia (pura y mixta) y de punto de equilibrio, así como su interpretación en situaciones concretas. \\
                                                                                B.2. Formas de representar un juego.\newline - Forma extensiva. Árbol del juego.\newline - Forma normal. Estrategias. Representación tabular del juego.\newline  &                                                                                   3.2. Utilizar la forma de representación apropiada para modelizar un juego o una situación determinada. \\
                                                                                                                                                                             B.3. Juegos de dos jugadores con suma cero\newline \newline  & 3.6. Expresar y comunicar los resultados de la resolución de un juego (ganancias, pérdidas, estrategias ganadores, etc.) en los términos del contexto concreto en que se está trabajando. \\
B.3. Juegos de dos jugadores con suma cero\newline - Resolución de juegos de dos jugadores y suma cero dados en forma normal. Estrategias puras, dominación y puntos silla. Estudio completo en el caso 2 × 2. Estrategias mixtas. &                                                                                   3.5. Resolver completamente juegos de dos jugadores y suma cero dados en forma normal en el caso 2 × 2. \\
                                                        B.3. Juegos de dos jugadores con suma cero\newline - Resolución de juegos de dos jugadores, suma cero e información perfecta dados en forma extensiva. Retropropagación.\newline  &                                                                                        3.4. Resolver juegos de dos jugadores, suma cero e información perfecta mediante retropropagación. \\
\end{longtable}
 
