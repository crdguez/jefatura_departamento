# Jefatura de departamento

<!---
your comment goes here
and here
-->


Documentación de jefatura de departamento del IES Pedro Cerrada

![logo ies pedro cerrada](./img/logo.jpeg)

## Normativa

* [Normativa ESO](https://educa.aragon.es/web/guest/-/normativa-eso)
* [Reglamento Orgánico de los Institutos de Educación Secundaria - Real Decreto 83/1996](https://www.boe.es/buscar/pdf/1996/BOE-A-1996-3834-consolidado.pdf) -Ver capítulo III para departamentos didácticos-
* [Organización y funcionamiento de los IES de la Comunidad Autónoma de Aragón](https://educa.aragon.es/documents/20126/521996/218+ORDEN+IOF+IES+boa%281%29.pdf/8cf0ff03-d0b5-b3f3-19c5-7ebf57d9de60?t=1578923050654)
* [Orden ECD/1172/2022, de 2 de agosto, por la que se aprueban el currículo y las características de la evaluación de la Educación Secundaria Obligatoria y se autoriza su aplicación en los centros docentes de la Comunidad Autónoma de Aragón](https://educa.aragon.es/documents/20126/2789389/ECD+1172+2022+de+2+de+agosto+%28curr%C3%ADculo+y+evaluaci%C3%B3n+ESO%29.pdf/8659291a-b7d9-66a8-b6d6-59d9c88d78b1?t=1661768667394)

## Tareas a inicio de curso

* Preparar la **hojas de inicio de curso**
* Preparar copias adicionales de los cuadernillos en caso de que se necesitaran
* Revisar las programaciones
* Preparar los libros de alumnos y profesores
* Solicitar acceso a los libros de anaya para los profesores
* Preparar el material de pendientes y actas
* Las actas las pongo en [https://gitlab.com/crdguez/mis_postits/-/tree/master/Curso%202324/Actas_departamento?ref_type=heads](https://gitlab.com/crdguez/mis_postits/-/tree/master/Curso%202324/Actas_departamento?ref_type=heads)

## Tareas Septiembre

* Convocar a los alumnos pendientes de bachillerato (1º). 20/09 --> Exámenes 13/12 y 20/03
* Subir a la web el procedimiento de recuperación de pendientes: [https://drive.google.com/drive/folders/10HftkXHhVDevSKwhHVJz_v-YCnBQA6qI](https://drive.google.com/drive/folders/10HftkXHhVDevSKwhHVJz_v-YCnBQA6qI)

## Tareas a final de curso

* Elaborar la memoria
* Solicitar la devolución de los libros a los profesores
* Realizar copias de los cuadernillos
