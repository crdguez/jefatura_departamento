---
title: |
	Resumen normativo 		
#date: 
institute: Departamento de Matemáticas
#documentclass: 
theme: Madrid
#theme: "Rochester"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
linkstyle: bold
urlcolor: red
aspectratio: 169

#header-includes: |
#	`\setbeamertemplate{section in toc}[round]`{=latex}
#	\setbeamerfont{section number projected}{size=\large}
#	\setbeamercolor{section number projected}{bg=red,fg=green} 

titlegraphic:  |
	`../img/logo_small.png`{=latex}
section-titles: true
toc: true
lang: es-EU
---

# Normativa
## Enlaces a la normativa 
* [Normativa ESO](https://educa.aragon.es/web/guest/-/normativa-eso)
* [Normativa Bahillerato](https://educa.aragon.es/web/guest/-/norma-bachillerato)
* [Reglamento Orgánico de los Institutos de Educación Secundaria - Real Decreto 83/1996](https://www.boe.es/buscar/pdf/1996/BOE-A-1996-3834-consolidado.pdf) -Ver capítulo III para departamentos didácticos-
* [Organización y funcionamiento de los IES de la Comunidad Autónoma de Aragón](https://educa.aragon.es/documents/20126/521996/218+ORDEN+IOF+IES+boa%281%29.pdf/8cf0ff03-d0b5-b3f3-19c5-7ebf57d9de60?t=1578923050654)
* [Orden ECD/1172/2022, de 2 de agosto, por la que se aprueban el currículo y las características de la evaluación de la Educación Secundaria Obligatoria y se autoriza su aplicación en los centros docentes de la Comunidad Autónoma de Aragón](https://educa.aragon.es/documents/20126/2789389/ECD+1172+2022+de+2+de+agosto+%28curr%C3%ADculo+y+evaluaci%C3%B3n+ESO%29.pdf/8659291a-b7d9-66a8-b6d6-59d9c88d78b1?t=1661768667394)
* [Orden ECD/1173/2022, de 3 de agosto, por la que se aprueban el currículo
  y las características de la evaluación del Bachillerato y se autoriza 
  su aplicación en los centros docentes de la comunidad autónoma de Aragón](https://educa.aragon.es/documents/20126/2789392/ECD+1173+2022+de+3+de+agosto+%28curr%C3%ADculo+y+evaluaci%C3%B3n+Bachillerato%29.pdf/df988f84-b700-ada0-4a95-98e060083e30?t=1661776021789)
* [ORDEN de 18 de mayo de 2015, de la Consejera de Educación, Universidad, Cultura y Deporte por la que se aprueban las Instrucciones que regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón.](https://educa.aragon.es/documents/20126/521996/218+ORDEN+IOF+IES+boa%281%29.pdf/8cf0ff03-d0b5-b3f3-19c5-7ebf57d9de60?t=1578923050654)
*[ORDEN ECD/779/2016, de 11 de julio, por la que se modifica el anexo de la Orden
de 18 de mayo de 2015, de la Consejera de Educación, Universidad, Cultura y Deporte, por la que se aprueban las Instrucciones que regulan la organización y el
funcionamiento de los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón](https://educa.aragon.es/documents/20126/521996/219+IOF+Secundaria.pdf/c0f5509d-c263-dafe-abb7-3e610a878251?t=1578923051681)

# Resumen normativo relativo a departamentos didácticos 

## Competencias de los departamentos didácticos BOE

Son competencias de los departamentos didácticos: 

a) Formular propuestas al equipo directivo y al claustro relativas a la elaboración o modificación del proyecto educativo del instituto y la programación general anual. 
b) Formular propuestas a la comisión de coordinación pedagógica relativas a la elaboración o modificación de los proyectos curriculares de etapa. 
c) Elaborar, antes del comienzo del curso académico, **la programación didáctica** de las enseñanzas correspondientes a las áreas, materias y módulos integrados en el departamento, bajo la coordinación y dirección del jefe del mismo, y de acuerdo con las directrices generales establecidas por la comisión de coordinación pedagógica. La programación didáctica incluirá, para cada etapa, los aspectos señalados en el artículo 68 de este Reglamento. 
d) Promover la investigación educativa y proponer actividades de perfeccionamiento de sus miembros. 
e) Mantener actualizada la metodología didáctica. 

---

f) Colaborar con el departamento de orientación, bajo la dirección del jefe de estudios, en la prevención y detección temprana de problemas de aprendizaje, y elaborar la programación y aplicación de adaptaciones curriculares para los alumnos que lo precisen, entre ellos los alumnos con necesidades educativas especiales y los que sigan programas de diversificación. 
g) Organizar y realizar **actividades complementarias** en colaboración con el departamento correspondiente. 
h) Organizar y realizar las **pruebas** necesarias para los alumnos de bachillerato o de ciclos formativos con materias o módulos **pendientes** y, en su caso, para los alumnos libres. 
i) Resolver las **reclamaciones** derivadas del proceso de evaluación que los alumnos formulen al departamento y dictar los informes pertinentes. 
j) Elaborar, a final de curso, una **memoria** en la que se evalúe el desarrollo de la programación didáctica, la práctica docente y los resultados obtenidos. 
k) Proponer **materias optativas** dependientes del departamento, que serán impartidas por los profesores del mismo. ([BOE, p. 21](zotero://select/library/items/4NHKIK3G))

## Competencias de los jefes de los departamentos didácticos BOE

Son competencias del jefe de departamento: 

a) Participar en la elaboración del proyecto curricular de etapa, coordinar la elaboración de la programación didáctica de las áreas, materias o módulos que se integran en el departamento y la memoria final de curso, así como redactar ambas.
b) Dirigir y coordinar las actividades académicas del departamento.
c) Convocar y presidir las reuniones ordinarias del departamento y las que, con carácter extraordinario, fuera preciso celebrar. 
d) **Elaborar y dar a conocer a los alumnos la información relativa a la programación**, con especial referencia a los objetivos, los mínimos exigibles y los criterios de evaluación. 
e) **Realizar las convocatorias, cuando corresponda, de los exámenes** para los alumnos de bachillerato o ciclos formativos con materias o módulos **pendientes**, alumnos libres, y de las pruebas extraordinarias, siempre en coordinación con la jefatura de estudios. **Presidir la realización de los ejercicios correspondientes y evaluarlos** en colaboración con los miembros del departamento. 

---

f) Velar por el cumplimiento de la programación didáctica del departamento y la correcta aplicación de los criterios de evaluación. 
g) **Resolver las reclamaciones de final de curso** que afecten a su departamento, de acuerdo con las deliberaciones de sus miembros, y elaborar los informes pertinentes. 
h) Coordinar la organización de espacios e instalaciones, **adquirir el material** y el equipamiento específico asignado al departamento, y velar por su mantenimiento. 
i) Promover la evaluación de la práctica docente de su departamento y de los distintos proyectos y actividades del mismo.  ([BOE, p. 22](zotero://select/library/items/4NHKIK3G))

## Organización de los Departamentos Didácticos BOA

* Las funciones y las competencias de dichos departamentos didácticos son las reguladas en el Título III del Reglamento Orgánico de los Institutos de Educación Secundaria. 
* Los Departamentos Didácticos celebrarán **reuniones semanales** que serán de obligada asistencia para todos sus miembros. **Al menos una vez al mes**, las reuniones de los Departamentos Didácticos tendrán por objeto **evaluar el desarrollo de la Programación didáctica y establecer las medidas correctoras** que esa evaluación aconseje, arbitrando medidas que permitan la adecuada coordinación didáctica entre los profesores de los Departamentos didácticos de las Secciones y de los Institutos de Educación Secundaria. **Lo tratado en estas reuniones será recogido en las actas** correspondientes redactadas por el Jefe del Departamento. Los Jefes de los Departamentos Didácticos unipersonales evaluarán el desarrollo de la Programación didáctica y establecerán las modificaciones oportunas, todo lo cual será recogido en un informe mensual. 

---

* Para hacer posible el cumplimiento de estas tareas y facilitar las reuniones periódicas entre los componentes de un mismo Departamento didáctico, el Jefe de Estudios, al confeccionar los horarios, reservará un periodo complementario a la semana en la que los miembros de un mismo Departamento Didáctico queden libres de otras actividades. Esta hora figurará en los respectivos horarios individuales.
* Al final del curso, los Departamentos Didácticos recogerán en una memoria la evaluación del desarrollo de la Programación Didáctica y los resultados obtenidos. **La memoria redactada por el Jefe de Departamento será entregada al Director antes del 30 de junio**, y será tenida en cuenta en la elaboración y, en su caso, en la revisión del Proyecto Curricular de Etapa y de la programación del curso siguiente." ([BOA, p. 20004](zotero://select/library/items/EQLCM8WD))

# Resumen normativo relativo a la Comisión de Coordinación Pedagógica

## Competencias de la comisión de coordinación pedagógica BOE

Son competencias de la CCP: 

a) Establecer las directrices generales para la elaboración y revisión de los **proyectos curriculares de etapa**. 
b) Supervisar la elaboración y revisión, así como coordinar y responsabilizarse de la redacción de los proyectos curriculares de etapa y su posible modificación, y asegurar su coherencia con el proyecto educativo del instituto. 
c) Establecer las directrices generales para la elaboración y revisión de las programaciones didácticas de los departamentos, del plan de orientación académica y profesional y del plan de acción tutorial, incluidos en el proyecto curricular de etapa. 
d) Proponer al claustro los proyectos curriculares para su aprobación. 
e) Velar por el cumplimiento y posterior evaluación de los proyectos curriculares de etapa.

---

f) Proponer al claustro la **planificación general de las sesiones de evaluación y calificación y el calendario de exámenes o pruebas extraordinarias**, de acuerdo con la jefatura de estudios. 
g) **Proponer al claustro de profesores el plan para evaluar el proyecto curricular de cada etapa, los aspectos docentes del proyecto educativo y la programación general anual**, la evolución del rendimiento escolar del instituto y el proceso de enseñanza. 
h) Fomentar la evaluación de todas las actividades y proyectos del instituto, colaborar con las evaluaciones que se lleven a cabo a iniciativa de los órganos de gobierno o de la Administración educativa e **impulsar planes de mejora** en caso de que se estime necesario, como resultado de dichas evaluaciones." ([BOE, p. 23](zotero://select/library/items/4NHKIK3G)) ([pdf](zotero://open-pdf/library/items/X227ID5M?page=23&annotation=2HUA2UNU))

## Competencias de la CCP BOA

1. La composición, la organización y las competencias de la Comisión de Coordinación Pedagógica son las establecidas en el Título III, Capítulo IV del Reglamento Orgánico de los Institutos de Educación Secundaria. En los centros con menos de nueve unidades, el Claustro de Profesores asumirá las funciones de la citada Comisión. 
2. La Comisión de Coordinación Pedagógica **se reunirá como mínimo una vez al mes** y celebrará **una sesión extraordinaria al comienzo del curso, otra al finalizar este** y cuantas otras se consideren necesarias. 
3. La Comisión de Coordinación Pedagógica deberá establecer las d**irectrices generales para la elaboración y revisión de los Proyectos Curriculares de Etapa y de las Programaciones didácticas**, incluidas en estos, con anterioridad al comienzo de la elaboración de dichas programaciones. Asimismo, la Comisión deberá establecer durante el mes de septiembre, y antes del inicio de las actividades lectivas, un calendario de actuaciones para el seguimiento y evaluación de los Proyectos Curriculares de Etapa y de las posibles modificaciones de los mismos, que puedan producirse como resultado de la evaluación, y solicitará del Servicio Provincial de Educación, Universidad, Cultura y Deporte el asesoramiento y apoyo que estimen oportunos. 

---

4. Durante el mes de septiembre y antes del inicio de las actividades lectivas, la Comisión de Coordinación Pedagógica propondrá al Claustro de Profesores, de acuerdo con la Jefatura de Estudios, **la planificación general de las sesiones de evaluación y calificación de los alumnos, así como el calendario de exámenes para su aprobación**. Esta planificación se incluirá además de en la Programación General Anual en el Plan de Acción Tutorial." ([BOA, p. 20005](zotero://select/library/items/EQLCM8WD))

# Programaciones didácticas

## Contenidos de la programación didáctica

### Programaciones de la ESO

Siguiendo las directrices de la ORDEN ECD/1172/2022, de 2 de agosto, por la que se aprueban el currículo y las características de la evaluación de la Educación Secundaria Obligatoria, las programaciones didácticas de cada curso incluirán, al menos, los siguientes aspectos en cada materia o ámbito:

a) **Competencias específicas** y **criterios de evaluación** asociados a ellas. 
b) Concreción, agrupamiento y secuenciación de los **saberes básicos** y **de los criterios de evaluación** en **unidades didácticas**. 
c) **Procedimientos e instrumentos de evaluación**, con especial atención al carácter formativo de la evaluación y a su vinculación con los criterios de evaluación. 
d) **Criterios de calificación**. 
e) Características de la evaluación inicial, criterios para su valoración, así como consecuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación. 
f) Actuaciones generales de atención a las diferencias individuales y adaptaciones curriculares para el alumnado que las precise. 
g) **Plan de seguimiento personal para el alumnado que no promociona**, de acuerdo con lo establecido en al artículo 19.4 de esta Orden 
h) **Plan de refuerzo personalizado para materias o ámbitos no superados**, de acuerdo con lo establecido en al artículo 20 de esta Orden.

----

i) **Estrategias didácticas y metodológicas:** Organización, recursos, agrupamientos, enfoques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios. 
j) **Concreción del Plan Lecto**r establecido en el Proyecto Curricular de Etapa. 
k) **Concreción del Plan de implementación de elementos transversales** establecido en el Proyecto Curricular de Etapa. 
l) **Concreción del Plan de utilización de las tecnologías digitales** establecido en el Proyecto Curricular de Etapa. 
m) En su caso, medidas complementarias que se plantean para el tratamiento de las materias o ámbitos dentro de proyectos o itinerarios bilingües o plurilingües, o de proyectos de lenguas y modalidades lingüísticas propias de la Comunidad Autónoma de Aragón. 
n) Mecanismos de revisión, evaluación y modificación de las programaciones didácticas en relación con los resultados académicos y procesos de mejora. 
ñ) **Actividades complementarias y extraescolares** programadas por cada departamento, equipos didáctico u órgano de coordinación didáctica que corresponda, de acuerdo con el programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado." ([BOA, p. 27864](zotero://select/library/items/KU7DTAKI))

### Programaciones de Bachillerato

Siguiendo las directrices de la ORDEN ECD/1173/2022, de 3 de agosto, por la que se aprueban el currículo y las características de la evaluación del Bachillerato, las programaciones didácticas de cada curso incluirán, al menos, los siguientes aspectos en cada materia: 

a) **Competencias específicas** y **criterios de evaluación** asociados a ellas.

b) Concreción, agrupamiento y secuenciación de los **saberes básicos** y de los **criterios de evaluación** en **unidades didácticas**. 

c) **Procedimientos e instrumentos de evaluación**, con especial atención al carácter formativo de la evaluación y a su vinculación con los criterios de evaluación. 

d) **Criterios de calificación**. 

e) Características de la evaluación inicial, criterios para su valoración, así como consecuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación. 

f) Actuaciones generales de atención a las diferencias individuales. 

g) **Plan de recuperación de materias pendientes**. 

----

h) **Estrategias didácticas y metodológicas**: Organización, recursos, agrupamientos, enfoques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios.

i) **Concreción del Plan de implementación de elementos transversales** establecido en el Proyecto Curricular de Etapa. 

j) **Concreción del Plan de utilización de las Tecnologías digitales** establecido en el Proyecto Curricular de Etapa. 

k) En su caso, medidas complementarias que se plantean para el tratamiento de las ma terias dentro de proyectos o itinerarios bilingües o plurilingües o de proyectos de lenguas y modalidades lingüísticas propias de la comunidad autónoma de Aragón. 

l) Mecanismos de revisión, evaluación y modificación de las programaciones Didácticas en relación con los resultados académicos y procesos de mejora. 

m) **Actividades complementarias y extraescolares** programadas por cada departamento, equipo u órgano de coordinación didáctica que corresponda, de acuerdo con el Programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.



## Elementos del currículo BOA

Definiciones de los elementos del currículo. A efectos de esta Orden se entenderá por: 

a) **Objetivos:** logros que se espera que el alumnado haya alcanzado al finalizar la etapa y cuya consecución está vinculada a la adquisición de las competencias clave y de las competencias específicas de las diferentes materias o ámbitos. 

b) **Competencias clave:** desempeños que se consideran imprescindibles para que el alumnado pueda progresar con garantías de éxito en su itinerario formativo, y afrontar los principales retos y desafíos globales y locales. Dichas competencias clave aparecen recogidas en el anexo I de esta Orden, definiendo el Perfil de salida del alumnado al término de la enseñanza básica, y son la adaptación al sistema educativo español de las competencias clave establecidas en la Recomendación del Consejo de la Unión Europea de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente. 

c) **Competencias específicas:** desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje requiere de los saberes básicos de cada materia o ámbito. Las competencias específicas constituyen un elemento de conexión entre, por una parte, el Perfil de salida del alumnado, y por otra, los saberes básicos de las materias o ámbitos y los criterios de evaluación. 

---

d) **Criterios de evaluación:** referentes que indican los niveles de desempeño esperados en el alumnado en las actividades o situaciones de aprendizaje a las que se refieren las competencias específicas de cada materia o ámbito en un momento determinado de su proceso de aprendizaje. 

e) **Saberes básicos:** conocimientos, destrezas y actitudes que constituyen los contenidos propios de una materia o ámbito cuyo aprendizaje es necesario para la adquisición de las competencias específicas. 

f) **Situaciones de aprendizaje**: situaciones y actividades que implican el despliegue por parte del alumnado de actuaciones que contribuyen al desarrollo y adquisición de las competencias clave y las competencias específicas y cuyo diseño involucra el aprendizaje de diferentes saberes básicos asociados a una o varias materias o ámbitos." ([BOA, p. 27836](zotero://select/library/items/KU7DTAKI))



## Currículo de Matemáticas

### Descriptores operativos de la competencias clave STEM

En cuanto a la dimensión aplicada de las competencias clave, se ha definido para cada una de ellas un conjunto de descriptores operativos, partiendo de los diferentes marcos europeos de referencia existentes

| Al completar la Educación Primaria, el alumno o la alumna... | Al completar la enseñanza básica, el alumno o la alumna...   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| STEM1. Utiliza, de manera guiada, algunos métodos inductivos y deductivos propios del razonamiento matemático en situaciones conocidas, y selecciona y emplea algunas estrategias para resolver problemas reflexionando sobre las soluciones obtenidas. | STEM1. Utiliza métodos inductivos y deductivos propios del razonamiento matemático en situaciones conocidas, y selecciona y emplea diferentes estrategias para resolver problemas analizando críticamente las soluciones y reformulando el procedimiento, si fuera necesario. |
| STEM2. Utiliza el pensamiento científico para entender y explicar algunos de los fenómenos que ocurren a su alrededor, confiando en el conocimiento como motor de desarrollo, utilizando herramientas e instrumentos adecuados, planteándose preguntas y realizando experimentos sencillos de forma guiada. | STEM2. Utiliza el pensamiento científico para entender y explicar los fenómenos que ocurren a su alrededor, confiando en el conocimiento como motor de desarrollo, planteándose preguntas y comprobando hipótesis mediante la experimentación y la indagación, utilizando herramientas e instrumentos adecuados, apreciando la importancia de la precisión y la veracidad y mostrando una actitud crítica acerca del alcance y las limitaciones de la ciencia. |
| STEM3. Realiza, de forma guiada, proyectos, diseñando, fabricando y evaluando diferentes prototipos o modelos, adaptándose ante la incertidumbre, para generar en equipo un producto creativo con un objetivo concreto, procurando la participación de todo el grupo y resolviendo pacíficamente los conflictos que puedan surgir. | STEM3. Plantea y desarrolla proyectos diseñando, fabricando y evaluando diferentes prototipos o modelos para generar o utilizar productos que den solución a una necesidad o problema de forma creativa y en equipo, procurando la participación de todo el grupo, resolviendo pacíficamente los conflictos que puedan surgir, adaptándose ante la incertidumbre y valorando la importancia de la sostenibilidad. |
| STEM4. Interpreta y transmite los elementos más relevantes de algunos métodos y resultados científicos, matemáticos y tecnológicos de forma clara y veraz, utilizando la terminología científica apropiada, en diferentes formatos (dibujos, diagramas, gráficos, símbolos...) y aprovechando de forma crítica, ética y responsable la cultura digital para compartir y construir nuevos conocimientos. | STEM4. Interpreta y transmite los elementos más relevantes de procesos, razonamientos, demostraciones, métodos y resultados científicos, matemáticos y tecnológicos de forma clara y precisa y en diferentes formatos (gráficos, tablas, diagramas, fórmulas, esquemas, símbolos...), aprovechando de forma crítica la cultura digital e incluyendo el lenguaje matemático-formal con ética y responsabilidad, para compartir y construir nuevos conocimientos. |
| STEM5. Participa en acciones fundamentadas científicamente para promover la salud y preservar el medio ambiente y los seres vivos, aplicando principios de ética y seguridad y practicando el consumo responsable. | STEM5. Emprende acciones fundamentadas científicamente para promover la salud física, mental y social, y preservar el medio ambiente y los seres vivos; y aplica principios de ética y seguridad en la realización de proyectos para transformar su entorno próximo de forma sostenible, valorando su impacto global y practicando el consumo responsable. |