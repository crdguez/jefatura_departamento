En el *Makefile* indicamos qué tiene que hacer. Con *make* lo hacemos, y con

 *make clean* borramos lo hecho.

Más info [aquí](https://ondiz.github.io/cursoLatex/Contenido/15.Pandoc.html)

