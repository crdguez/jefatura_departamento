## Procedimientos e instrumentos de evaluación, con especial atención al carácter formativo de la evaluación y a su vinculación con los criterios de evaluación

Para la evaluación de los alumnos se utilizarán los siguientes instrumentos:

- Cuaderno personal del alumno (expresión escrita, elaboración de conclusiones y resúmenes, corrección personal de los controles y actividades realizadas, métodos de trabajo).

- Observación en el aula (hábitos de trabajo, comunicación lógica de sus pensamientos y dificultades, capacidades de tipo intelectual, interés, motivación, concentración, atención, conceptos mal aprendidos, aceptación del trabajo cooperativo).

- Trabajos de investigación (utilización de la información recogida y del material de clase, exposición y comunicación de los resultados, uso de instrumentos matemáticos y el contenidio matemático, corrección de los resultados y conclusiones, la toma de decisiones, diseño global).

- Pruebas escritas. Se recogerá información sobre: conocimientos básicos de la unidad, utilización de los diferentes niveles y métodos de razonamiento, técnicas instrumentales.

Los resultados de las diferentes valoraciones se darán a conocer a los alumnos. En las pruebas especificas además de indicar los errores cometidos y la nota obtenida, se apuntarán, en caso necesario, las observaciones y alternativas oportunas para superarlas.

Se acuerda en el departamento que para los alumnos de 3º y 4º ESO opción B se realizarán dos exámenes por evaluación, uno a mitad de la misma y otro al finalizar la evaluación donde se incluirán la totalidad de contenidos de la evaluación, y que tendrá una puntuación doble que el primer examen.



## Criterios de calificación.

### Criterios generales

De manera general, se seguirán los siguientes criterios

- En los ejercicios básicos de consolidación de los procedimientos se pedirá exactitud en los resultados.            

- Para superar positivamente una prueba sobre procedimientos básicos será necesario demostrar un buen dominio de ellos y no sólo realizar correctamente un determinado porcentaje de los ejercicios.

- En la calificación de un problema se valorará sobre todo el proceso seguido y no tanto los errores de cálculo, salvo reiteración. En tal caso se tendrá en cuenta el conocimiento acumulado por el profesor sobre el alumno a lo largo del curso.

- No se aceptarán resultados numéricos sin que quede reflejado el proceso de obtención de los mismos.

- En las pruebas escritas se valorará la limpieza y claridad en la expresión y el buen uso de los formalismos matemáticos.

- Un alumno que haya faltado a clase deberá realizar las actividades correspondientes a esos días para ser evaluado positivamente.Para ser evaluado positivamente es necesario haber completado el cuaderno de clase con la suficiente corrección.

- Los resultados de una evaluación sumativa o final se expresarán con una nota que tendrá una escala del 0 al 10. 

- La nota numérica obtenida se transformará a calificación siguiendo la siguiente tabla:

   

  | Nota numérica | Calificación  |
  | ------------- | ------------- |
  | [0,5)         | Insuficiente  |
  | [5,6)         | Suficiente    |
  | [6,7)         | Bien          |
  | [7,9)         | Notable       |
| [9,10]        | Sobresaliente |
  
  

### Criterios específicos de la asignatura de Matemáticas

Para homogeneizar las valoraciones de cada uno de los ítems utilizados en la calificación final del alumnado hemos llegado en el Departamento a los siguientes acuerdos:             

La participación en clase, revisión de las tareas hechas en casa, y el trabajo de clase, así como las actividades complementarias que se van realizando será como máximo un 30% de la nota (actitud, cuaderno  y trabajo en clase valorada de 1 a 10 supondrá un 20% y revisión de donde se incluyen todas las tareas que el alumnado debe realizar fuera del aula (deberes diarios, hojas de repaso de problemas, trabajos puntuales,….) hasta otro 10%. Las pruebas individuales tendrán un valor mínimo del  70%.             

El peso de cada uno de los apartados en cada curso es el siguiente:

|                       | Nota examen | Tareas de casa | Trabajo clase, cuaderno y actitud |
| :-------------------: | ----------- | -------------- | --------------------------------- |
|        1º ESO         | 70%         | 20%            | 10%                               |
|        2º ESO         | 75%         | 15%            | 10%                               |
|        3º ESO         | 85%         | 10%            | 5%                                |
| 4º ESO Matemáticas A  | 70-90%      | 5-10%          | 5-20%                             |
| 4º ESO  Matemáticas B | 90%         | 5%             | 5%                                |


### Criterios específicos de las asignaturas optativas Laboratorio de refuerzo de competencias y  Matemáticas para la toma de decisiones

En la calificación de los alumnos se tendrá en cuenta:             

- Actividades realizadas en el aula
- Cuaderno personal
- Actividades individuales
- Actividades en equipo
- Actividades propuestas para realizar en casa
- Actividades relacionadas con los contenidos.
- Actividades de creación.
- Otras actividades que puedan surgir.
- Realización de exámenes, pruebas o controles.
- Se valorará la actitud y el interés del alumno en la materia

Es necesario  para superar la materia optativa demostrar una actitud de esfuerzo continuado a lo largo de toda la evaluación y, por extensión, de todo el curso.                       

Un alumno que haya faltado a clase deberá realizar las actividades correspondientes a esos días para ser evaluado.             

En las pruebas escritas se tendrá en cuenta la limpieza y claridad en la expresión.             
No se otorgará el aprobado a un alumno que no haya entregado el cuaderno de clase o sin la suficiente corrección.             

Por otra parte, también será necesario superar los exámenes realizados cuyos criterios de calificación pormenorizará cada profesor con anterioridad.             

La calificación obtenida por el alumno en la optativa de Laboratorio de refuerzo de competencias, será la nota media de la calificación obtenida en ambas competencias por separado, siendo necesaria una calificación mínima de 3 en cada una de ellas para poder realizar dicha media.             


## Características de la evaluación inicial, criterios para su valoración, así como consecuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación

La evaluación inicial se realizará al comienzo de cada curso. Su finalidad es que los profesores conozcamos las características de cada alumno para obtener el punto inicial en cada curso y/o grupo del desempeño de nuestra labor docente a lo largo del curso.             

Las diferentes formas de llevar a cabo esta evaluación inicial de nuestro alumnado:             

- Realización de una prueba inicial, la cual se elaborará teniendo en cuenta los conocimientos previos que tienen que tener nuestros alumnos; es decir, esta prueba tiene que partir de los objetivos y contenidos mínimos que el alumno debió adquirir al finalizar el curso anterior.

- Observación sistemática en el aula, la cual se hará mediante la realización de ejercicios básicos que ellos ya deben saber resolver (hay que tener en cuenta que en todos los cursos se empieza por temas que prácticamente son de repaso y que se basan en los conocimientos adquiridos en cursos anteriores)

- Información de los profesores que les han dado clase el curso anterior.            

El departamento elabora una prueba inicial común para todos los alumnos de 1º ESO al ser nuevos en el centro y disponer solo de la información proveniente de los informes de primaria             

En los demás cursos se utilizará principalmente la observación sistemática en el aula y los informes que nos dan los profesores del curso anterior.   

A mitad de octubre la información que sea relevante del proceso anterior será transmitida al equipo educativo en las correspondientes evaluaciones predictivas.          

## Actuaciones generales de atención a las diferencias individuales y adaptaciones curriculares para el alumnado que las precise 

Para el presente curso se ha previsto una adaptación curricular significativa al alumno Nathan Bertolini. Dicha adaptación se adjunta como anexo a esta programación.

## Plan de seguimiento personal para el alumnado que no promociona 

Para el alumno que no promocione con la asignatura pendiente se tiene previsto hacer el mismo seguimiento que con los alumnos que sí han promocionado pero que se detecten dificultades de aprendizaje. 

Para estos alumnos se podrán tomar diferentes medidas:

- Trabajar con fichas específicas que refuercen los contenidos
- Informar al tutor o tutora, vía correo electrónico u otro medio, los resultados parciales que vayan obteniendo a lo largo del curso. En el caso de detectar dificultades socioafectivas o curriculares en dicho alumno, se podrán incluir las observaciones que se consideren pertinentes.
- Realizar propuestas de participación en el programas de refuerzo educativo AÚNA

En el presente curso el Departamento no dispone de horas para poder realizar apoyos o refuerzos
educativos dentro del aula. En caso de haberlos tenido,  habrían sido aplicados a los cursos primeros de la ESO con el objetivo de evitar posteriores problemas de base.

## Plan de refuerzo personalizado para materias o ámbitos no superados 

Para recuperar las matemáticas pendientes de primero, segundo o tercero de la ESO se seguirá el siguiente procedimiento:

- Durante los exámenes del curso actual se seleccionarán una serie de ejercicios para evaluar el grado de adquisición de los conocimientos de  la materia pendiente. Los ejercicios que se tendrán en cuenta para el apartado de pendientes serán indicados al principio del examen dentro de las instrucciones del mismo. Los ejercicios seleccionados se corresponderán siempre con contenidos del curso pendiente.
- En cada examen del alumno con las matemáticas pendientes, aparecerán dos notas. Una correspondiente al examen del curso actual y otra correspondiente a la evaluación de las matemáticas pendientes.
- Para superar las matemáticas pendientes se deberá obtener una calificación (nota media de los exámenes de 5) positiva, obteniendo como mínimo un 3,5 en cada uno de los exámenes.
- Para aquellos alumnos/as que no superen la asignatura, se realizará un examen final (todos los contenidos del curso) en día y fecha determinado por Jefatura de Estudios.

## 

## Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfoques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios

Las estrategias didácticas y metodológicas son imprescindibles para un
adecuado desarrollo de las competencias.
La consecución de las diferentes dimensiones de la competencia matemática
tiene como finalidad que el individuo sea capaz de razonar matemáticamente y
de formular, emplear e interpretar las matemáticas para resolver problemas
presentes en los contextos de la vida real. Sin embargo, la resolución de
problemas no es únicamente un objetivo de las matemáticas, sino que se
identifica también como un enfoque metodológico para el aprendizaje de las
mismas. Este tipo de tareas exigen comprensión y autorregulación del propio proceso cognitivo, puesto que el alumnado debe analizar las diferentes
estrategias o caminos de resolución, lo que implica la toma de decisión y, por
tanto, se favorece la autonomía del alumnado. Un enfoque próximo a la
resolución de problemas centra el interés en el proceso y no en el resultado. Este
hecho exige una reflexión sobre la visión acerca del error, donde se concibe
como parte fundamental del proceso de aprendizaje. En dicho proceso, el
alumnado deberá poner en juego capacidades matemáticas como modelizar,
interpretar resultados, formular conjeturas, argumentar y razonar inductiva y
deductivamente, utilizar de diferentes representaciones, comunicar los
resultados, y establecer conexiones entre diferentes saberes matemáticos y con
saberes de otras disciplinas.
Además, la resolución de problemas proporciona oportunidades al docente
para dar respuesta a la dimensión afectiva.

Un aspecto importante en los procesos de enseñanza y aprendizaje de las
matemáticas son los recursos:

- Recursos físicos (libros de texto, cuaderno del alumnado, pizarra,
materiales manipulativos, lecturas de contenido matemático y prensa).

- Recursos digitales (pizarra digital interactiva, software informático
matemático específico, apps educativas, blogs, recursos audiovisuales
como cine, películas, series, vídeos...)
- Recursos transversales (juegos matemáticos, historia de la matemática omo recurso didáctico, el propio entorno y los paseos matemáticos...).

Además, el trabajo en equipo permite a través de la sociabilización enriquecer y
dar respuesta a las dificultades personales a través de la puesta en común y
reflexión sobre las diferentes estrategias. Asimismo, se puede atender las
diferencias individuales con apoyos o facilitadores del aprendizaje como los
materiales manipulativos. El trabajo en grupo debe garantizar la puesta en
común de ideas donde se compartan los significados personales construidos y
estrategias diseñadas. Por tanto, el interés recae en la interacción como medio
para construir conocimiento matemático situando el foco en el proceso y no en
el producto final.
Las situaciones de aprendizaje aparecen en la LOMLOE como herramientas
eficaces para integrar los elementos curriculares de las distintas materias o
ámbitos mediante tareas y actividades significativas y relevantes para resolver
problemas de manera creativa y cooperativa, reforzando la autoestima, la
autonomía, la reflexión crítica y la responsabilidad. 

Para la ESO se utilizan los siguientes libros:


|Curso|Recurso|
|:----|:----|
|1º ESO|Matemáticas 1  ISBN: 978-84-143-0528-7|
|2º ESO|Matemáticas 2 ISBN: 978-84-143-2495-0|
|3º ESO|Matemáticas 3 ISBN: 978-84-143-0532-4|
|4º ESO A|Matemáticas A 4 ISBN: 978-84-143-2551-3|
|4º ESO B|Matemáticas B 4 ISBN:978-84-143-2499-8|
|Laboratorios de Competencias|Apuntes y fichas de trabajo|
|Matemáticas para la toma de decisiones|Apuntes y fichas de trabajo|





## Concreción del Plan Lector establecido en el Proyecto Curricular de Etapa. 

Con objeto de mejorar la comprensión lectora, y  tanto la expresión oral como escrita se podrán seguir diferentes estrategias:

* Utilizar correctamente el lenguaje matemático.
* En los problemas explicar a los compañeros los pasos que han seguido para su resolución.
* Lecturas matemáticas. Dedicar alguna sesión al trimestre para la lectura de un libro relacionado con las
  matemáticas.
* Exposición oral y escrita de la interpretación de gráficas y estadísticas.

## Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa 

Los elementos transversales constituyen unos saberes comunes que hay que desarrollar entre todas las materias para la adquisición de las competencias clave y la consecución de los objetivos de la etapa. Por tanto, a lo largo de todas las unidades y las materias vinculadas al departamento se tendrán en cuenta  fomentar y potenciar los siguientes aspectos: 

* La comprensión lectora y la expresión oral y escrita
* La comunicación audiovisual
* La competencia digital
* El emprendimiento social y empresarial
* El fomento del espíritu crítico y científico
* La educación emocional y en valores
* La igualdad de género
* La creatividad
* La educación para la salud, incluida la afectivo-sexual
* La formación estética
* La educación para la sostenibilidad y el consumo responsable
* El respeto mutuo y la cooperación entre iguales

## Concreción del Plan de utilización de las tecnologías digitales establecido en el Proyecto Curricular de Etapa. 

La ley de educación LOMLOE, en el desarrollo de la materia de Matemáticas destaca la importancia de las nuevas tecnologías, y el uso habitual de aplicaciones y programas informáticos en el aula: Desarrollo de pensamiento computacional para la resolución de problemas, desarrollo del sentido espacial mediante programas de geometría dinámica, ... 

Entre las herramientas utilizadas por el departamento destacan:

* Geogebra
* Blockscad
* SageMath

Para poder hacer uso de ellas en el aula, el centro dispone de miniportátiles.

## En su caso, medidas complementarias que se plantean para el tratamiento de las materias o ámbitos dentro de proyectos o itinerarios bilingües o plurilingües, o de proyectos de lenguas y modalidades lingüísticas propias de la Comunidad Autónoma de Aragón. 

Todas la materias del departamento son impartidas en castellano, por lo que no se toman medidas en este apartado

## Mecanismos de revisión, evaluación y modificación de las programaciones didácticas en relación con los resultados académicos y procesos de mejora. 

Durante todo el curso, en las reuniones de Departamento, se irá controlando el desarrollo de la Programación. También se irá tomando nota de todas las sugerencias que vayan surgiendo con el
objetivo de velar por el ajuste y calidad de nuestra programación.

Al final de cada trimestre se revisará el desarrollo de la programación así como los resultados
académicos obtenidos, recogiendo las modificaciones que se estimen necesarias. Se reflejará por parte
de los miembros del departamento los resultados académicos en los grupos donde da clase, así como
los contenidos impartidos, utilizándolos de documento base para la realización del análisis. Se tratará en
lo posible de mejorar el rendimiento de los alumnos analizando los diferentes resultados entre grupos
de un mismo nivel, valorando el rendimiento de los mismos.

El departamento realiza una valoración de la práctica docente y del proceso de enseñanza aprendizaje
al final del curso que será incluida en su memoria final.

## Actividades complementarias y extraescolares programadas por cada departamento, equipos didáctico u órgano de coordinación didáctica que corresponda, de acuerdo con el programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.

Para este curso el Departamento de Matemáticas no tiene previsto realizar las actividades
complementarias y extraescolares que eran características del mismo.

