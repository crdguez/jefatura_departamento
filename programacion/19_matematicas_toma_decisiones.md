## Matemáticas orientadas a la toma de decisiones 4º ESO

### Competencias específicas y criterios de evaluación asociados
a ellas

#### CE.MTD.1 Reconocer la importancia de la aritmética modular en un contexto tecnológico y digital, comprendiendo la necesidad y  los fundamentos básicos de algoritmos de codificación sencillos y siendo capaz de aplicarlos de forma efectiva en situaciones concretas.

##### Criterios de evaluación

1.1. Aplicar el algoritmo de Euclides para calcular el m.c.d. de dos números y para obtener la expresión de la identidad de Bezout.
1.2. Resolver ecuaciones diofánticas lineales en una y dos variables, estudiando previamente la existencia de solución.
1.3. Poseer los fundamentos necesarios para trabajar módulo un entero m, sabiendo las diferentes propiedades que surgen según m sea primo o no.
1.4. Resolver de forma constructiva sistemas de congruencias lineales con una incógnita, estudiando previamente la existencia de solución.
1.5. Conocer y determinar unidades y divisores de cero en Z/mZ para cualquier m.
1.6. Aplicar el pequeño teorema de Fermat para estudiar la primalidad de un entero dado.
1.7. Conocer, idear y aplicar algoritmos de cifrado de sustitución y polialfabéticos sencillos, entendiendo sus vulnerabilidades.
1.8. Conocer los fundamentos y vulnerabilidades del algoritmo RSA, aplicándolo en casos sencillos. 

#### Identificar la utilidad de la teoría de grafos para modelizar situaciones y problemas reales de la vida cotidiana y de materias del ámbito científico y tecnológico, empleándola para explorar distintas formas de proceder y para obtener y comunicar posibles soluciones.

##### Criterios de evaluación

