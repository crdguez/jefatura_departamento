##  Procedimientos e instrumentos de evaluación, con especial atención al carácter formativo de la evaluación y a su vinculación con los criterios de evaluación

La apreciación del profesor acerca del progreso de los alumnos, lo que convierte a la evaluación en un
proceso cotidiano, continuo y dinámico.
Las pruebas escritas nos informarán sobre la adquisición de contenidos por parte de los alumnos.
También ofrecen una valiosa información sobre su actitud matemática los trabajos escritos, las
investigaciones y trabajos a largo plazo, tareas para realizar fuera del aula, las exposiciones orales
Es importante desarrollar en el alumno la actitud crítica sobre su propio trabajo y el de sus compañeros.
Conviene incorporar al proceso de evaluación del aprendizaje la opinión del sujeto activo del mismo,
mediante técnicas de autoevaluación.

##  Criterios de calificación

En la calificación de los alumnos se tendrá en cuenta:      

* En los ejercicios básicos de consolidación de los procedimientos se pedirá exactitud en los resultados.

* Para superar positivamente una prueba sobre procedimientos básicos será necesario demostrar un buen dominio de ellos y no sólo realizar correctamente un determinado porcentaje de los ejercicios.

* En la calificación de un problema se valorará sobre todo el proceso seguido y no tanto los errores de
  cálculo, salvo reiteración. En tal caso se tendrá en cuenta el conocimiento acumulado por el profesor sobre el alumno a lo largo del curso. 

* No se aceptarán resultados numéricos sin que quede reflejado el proceso de obtención de los mismos.

* En las pruebas escritas se valorará la limpieza y claridad en la expresión y el buen uso de los formalismos matemáticos.

* En cada una de las evaluaciones la nota se obtendrá del examen realizado al final del trimestre que
  constará de la totalidad de los contenidos desarrollados en dicho trimestre, si así fuera acordado en el
  departamento a principio de curso se podrá realizar un examen a mitad de trimestre (no supondrá
  eliminación de materia), y se valorará entre un 15 y 30 % de la nota de cada evaluación.

* Al finalizar el curso se realizará un examen final de todos los contenidos del curso, que servirá para ajustar la nota obtenida por los alumnos, podrá servir para que los alumnos aprueben la asignatura, pero en ningún caso se tomará como único examen realizado en el curso por los alumnos.

* Como situación excepcional, si a un alumno tuviera un solo bloque de contenidos no superado, se podrá presentar en el examen final únicamente a dicho bloque, y si opta por presentarse al final, se extraerá una nota del bloque no superado aparte de la nota del examen final.

  

La calificación final se obtendrá de la siguiente forma:

* 1º Bachillerato Tecnológico: Será la nota media de los cuatro bloques de contenidos (Aritmética y Álgebra, Geometría plana, Estadística y probabilidad y Análisis).
* 1º Bachillerato de Ciencias Sociales: Será la nota media de los tres bloques de contenidos (Aritmética y Álgebra, Estadística y Probabilidad y Análisis).
* 2º Bachillerato de Ciencias Sociales: Será la nota media de los tres bloques de contenidos (Aritmética y Álgebra, Estadística y Probabilidad y Análisis).
* 2º Bachillerato Tecnológico: Será la nota media ponderada de los cuatro bloques de contenidos (Álgebra (30%), Geometría(15%), Probabilidad (10%) y Análisis (40%)).

##  Características de la evaluación inicial, criterios para su valoración, así como consecuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación

La evaluación inicial se realizará al comienzo de cada curso. Su finalidad es que los profesores conozcamos las características de cada alumno para obtener el punto inicial en cada curso y/o grupo del desempeño de nuestra labor docente a lo largo del curso.             

Para  llevar a cabo la evaluación inicial de nuestro alumnado, el profesorado utilizará las siguientes herramientas:             

- Observación sistemática en el aula, la cual se hará mediante la realización de ejercicios básicos que ellos ya deben saber resolver (hay que tener en cuenta que en todos los cursos se empieza por temas que prácticamente son de repaso y que se basan en los conocimientos adquiridos en cursos anteriores). Se hará un seguimiento especial a aquellos alumnos de nueva incorporación al centro y que se suelen dar en 1º de Bachilleratos
- Información de los profesores que les han dado clase el curso anterior.            

A mitad de octubre la información que sea relevante del proceso anterior será transmitida al equipo educativo en las correspondientes evaluaciones predictivas.

##  Actuaciones generales de atención a las diferencias individuales

El alumnado con necesidades específicas de apoyo educativo será evaluado con
las adaptaciones de tiempo y medios apropiados a sus posibilidades y
características, incluyendo el uso de sistemas de comunicación alternativos y la
utilización de apoyos técnicos que faciliten el proceso de evaluación o adaptación
formal de los instrumentos de evaluación.

##  Plan de recuperación de materias pendientes

Para los alumnos con las matemáticas de 1º pendientes se seguirá el siguiente plan:

Se realizarán dos exámenes a lo largo del curso, previa realización de los ejercicios que serán entregados a los alumnos en las fechas indicadas.

El profesor de la asignatura y el jefe de departamento se encargarán de orientar el proceso de aprendizaje a lo largo del curso.

**1^er^ EXAMEN**

- En septiembre reunión con los alumnos de 2º bachillerato con el área de matemáticas pendiente. Se les entregará las  hojas de ejercicios correspondientes al primer examen, y se les darán las instrucciones para su realización.

- El examen se realizará a final de diciembre.

  

**CONTENIDOS DEL 1^er^ EXAMEN ** 

* MATEMÁTICAS I
    - Aritmética y Álgebra
    - Trigonometría

* MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I
    - Aritmética y Álgebra
    - Estadística

**2º EXAMEN**

- Al finalizar el primer examen se entregarán las hojas correspondientes al segundo examen, y se darán las instrucciones para su realización.
- El examen se realizará en Marzo

**CONTENIDOS DEL 2º EXAMEN**

* MATEMÁTICAS I
    - Números complejos
    - Estadística y Probabilidad
    - Geometría analítica
    - Funciones

* MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I
    - Probabilidad
    - Funciones.

La calificación final se obtendrá como media aritmética de los dos exámenes, siempre y cuando se alcance una calificación mínima de 3 en cada uno de ellos.

Para aquellos alumnos/as que no superen la asignatura, se realizará un examen final (todos los contenidos del curso) en día y fecha determinado por Jefatura de Estudios.

##  Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfoques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios

Las estrategias didácticas y metodológicas son imprescindibles para un
adecuado desarrollo de las competencias.
La consecución de las diferentes dimensiones de la competencia matemática
tiene como finalidad que el individuo sea capaz de razonar matemáticamente y
de formular, emplear e interpretar las matemáticas para resolver problemas
presentes en los contextos de la vida real. Sin embargo, la resolución de
problemas no es únicamente un objetivo de las matemáticas, sino que se
identifica también como un enfoque metodológico para el aprendizaje de las
mismas. Este tipo de tareas exigen comprensión y autorregulación del propio proceso cognitivo, puesto que el alumnado debe analizar las diferentes
estrategias o caminos de resolución, lo que implica la toma de decisión y, por
tanto, se favorece la autonomía del alumnado. Un enfoque próximo a la
resolución de problemas centra el interés en el proceso y no en el resultado. Este
hecho exige una reflexión sobre la visión acerca del error, donde se concibe
como parte fundamental del proceso de aprendizaje. En dicho proceso, el
alumnado deberá poner en juego capacidades matemáticas como modelizar,
interpretar resultados, formular conjeturas, argumentar y razonar inductiva y
deductivamente, utilizar de diferentes representaciones, comunicar los
resultados, y establecer conexiones entre diferentes saberes matemáticos y con
saberes de otras disciplinas.
Además, la resolución de problemas proporciona oportunidades al docente
para dar respuesta a la dimensión afectiva.

Un aspecto importante en los procesos de enseñanza y aprendizaje de las
matemáticas son los recursos:

- Recursos físicos (libros de texto, cuaderno del alumnado, pizarra,
  materiales manipulativos, lecturas de contenido matemático y prensa).
- Recursos digitales (pizarra digital interactiva, software informático
  matemático específico, apps educativas, blogs, recursos audiovisuales
  como cine, películas, series, vídeos...)
- Recursos transversales (juegos matemáticos, historia de la matemática omo recurso didáctico, el propio entorno y los paseos matemáticos...).

Además, el trabajo en equipo permite a través de la sociabilización enriquecer y
dar respuesta a las dificultades personales a través de la puesta en común y
reflexión sobre las diferentes estrategias. Asimismo, se puede atender las
diferencias individuales con apoyos o facilitadores del aprendizaje como los
materiales manipulativos. El trabajo en grupo debe garantizar la puesta en
común de ideas donde se compartan los significados personales construidos y
estrategias diseñadas. Por tanto, el interés recae en la interacción como medio
para construir conocimiento matemático situando el foco en el proceso y no en
el producto final.
Las situaciones de aprendizaje aparecen en la LOMLOE como herramientas
eficaces para integrar los elementos curriculares de las distintas materias o
ámbitos mediante tareas y actividades significativas y relevantes para resolver
problemas de manera creativa y cooperativa, reforzando la autoestima, la
autonomía, la reflexión crítica y la responsabilidad.

Para el Bachillerato se utilizan los siguientes libros:

|Curso|Recurso|
|:----|:----|
|1º Bachillerato CCSS|Matemáticas aplicadas a las Ciencias Sociales I ISBN: 978-84-143-1114-1|
|1º Bachillerato CIENCIAS|Apuntes del departamento|
|2º Bachillerato CCSS|Apuntes del departamento|
|2º Bachillerato CIENCIAS|Apuntes del departamento|

##  Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa

Los elementos transversales constituyen unos saberes comunes que hay que desarrollar entre todas las materias para la adquisición de las competencias clave y la consecución de los objetivos de la etapa. Por tanto, a lo largo de todas las unidades y las materias vinculadas al departamento se tendrán en cuenta  fomentar y potenciar los siguientes aspectos: 

* Comprensión lectora 
* Expresión oral y escrita 
* Comunicación audiovisual y competencia digital
* Fomento de creatividad y espíritu crítico 
* Emprendimiento 
* Consumo responsable y desarrollo sostenible 
* Educación para la salud, incluida la afectivo-sexual.
* Educación emocional y en valores: igualdad de género, educación para la paz, autonomía, reflexión, ...

##  Concreción del Plan de utilización de las Tecnologías digitales establecido en el Proyecto Curricular de Etapa

La ley de educación LOMLOE, en el desarrollo de la materia de Matemáticas destaca la importancia de las nuevas tecnologías, y el uso habitual de aplicaciones y programas informáticos en el aula: Desarrollo de pensamiento computacional para la resolución de problemas, desarrollo del sentido espacial mediante programas de geometría dinámica, ... 

Entre las herramientas utilizadas por el departamento destacan:

- Geogebra
- Blockscad
- SageMath

Para poder hacer uso de ellas en el aula, el centro dispone de miniportátiles.

##  En su caso, medidas complementarias que se plantean para el tratamiento de las materias dentro de proyectos o itinerarios bilingües o plurilingües o de proyectos de lenguas y modalidades lingüísticas propias de la comunidad autónoma de Aragón

Todas la materias del departamento son impartidas en castellano, por lo que no se toman medidas en este apartado

##  Mecanismos de revisión, evaluación y modificación de las programaciones Didácticas en relación con los resultados académicos y procesos de mejora

Durante todo el curso, en las reuniones de Departamento, se irá controlando el desarrollo de la Programación. También se irá tomando nota de todas las sugerencias que vayan surgiendo con el
objetivo de velar por el ajuste y calidad de nuestra programación.

Al final de cada trimestre se revisará el desarrollo de la programación así como los resultados
académicos obtenidos, recogiendo las modificaciones que se estimen necesarias. Se reflejará por parte
de los miembros del departamento los resultados académicos en los grupos donde da clase, así como
los contenidos impartidos, utilizándolos de documento base para la realización del análisis. Se tratará en
lo posible de mejorar el rendimiento de los alumnos analizando los diferentes resultados entre grupos
de un mismo nivel, valorando el rendimiento de los mismos.

El departamento realiza una valoración de la práctica docente y del proceso de enseñanza aprendizaje
al final del curso que será incluida en su memoria final.

##  Actividades complementarias y extraescolares programadas por cada departamento, equipo u órgano de coordinación didáctica que corresponda, de acuerdo con el Programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.

Para este curso el Departamento de Matemáticas no tiene previsto realizar las actividades
complementarias y extraescolares que eran características del mismo.