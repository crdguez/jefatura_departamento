---
# Datos
title: Resumen Programación de Matemáticas
subtitle: Curso 2023-2024
author: IES Pedro Cerrada
lang: es

# Control
toc: False
#theme: Madrid
theme: Pittsburgh
mainfont: LiberationSans

fontsize: 9pt

#bibliography: referencias.bib
#csl: formato.csl
#link-citations: true
---

# Programación ESO: Criterios de calificación

El peso de cada uno de los apartados en cada curso es el siguiente:

|                       | Nota examen | Tareas de casa | Trabajo clase, cuaderno y actitud |
| :-------------------: | ----------- | -------------- | --------------------------------- |
|        1º ESO         | 70%         | 20%            | 10%                               |
|        2º ESO         | 75%         | 15%            | 10%                               |
|   3º ESO    | 85%         | 10%            | 5%                                |
| 4º ESO Matemáticas A  | 70-90%      | 5-10%          | 5-20%                             |
| 4º ESO  Matemáticas B | 90%         | 5%             | 5%                                |

# Programación ESO: Plan de recuperación 

Para recuperar las matemáticas pendientes de primero, segundo o tercero de la ESO se seguirá el siguiente procedimiento:

- Durante los exámenes del curso actual se seleccionarán una serie de ejercicios para evaluar el grado de adquisición de los conocimientos de  la materia pendiente. Estos serán **indicados al principio del examen** dentro de las instrucciones del mismo. Los ejercicios seleccionados se corresponderán siempre con contenidos del curso pendiente.
- En cada examen del alumno con las matemáticas pendientes, aparecerán dos notas. Una correspondiente al examen del curso actual y otra correspondiente las matemáticas pendientes.
- Para superar las matemáticas pendientes se deberá obtener una calificación (**nota media de los exámenes de 5)** positiva, obteniendo como mínimo **un 3,5 en cada uno de los exámenes**.
- Para aquellos alumnos/as que no superen la asignatura, se realizará un **examen final** (todos los contenidos del curso) en día y fecha determinado por Jefatura de Estudios.



# Programación Bachillerato: Criterios de calificación

En cada una de las evaluaciones la nota se obtendrá del examen realizado al final del trimestre que
constará de la totalidad de los contenidos desarrollados en dicho trimestre, si así fuera acordado en el
departamento a principio de curso se podrá realizar un **examen a mitad de trimestre** (no supondrá
eliminación de materia), y **se valorará entre un 15 y 30 %** de la nota de cada evaluación.

La calificación final se obtendrá de la siguiente forma:

- **1º Bachillerato Tecnológico:** Será la nota media de los cuatro bloques de contenidos (Aritmética y Álgebra, Geometría plana, Estadística y probabilidad y Análisis).

- **1º Bachillerato de Ciencias Sociales:** Será la nota media de los tres bloques de contenidos (Aritmética y Álgebra, Estadística y Probabilidad y Análisis).

- **2º Bachillerato de Ciencias Sociales:** Será la nota media de los tres bloques de contenidos (Aritmética y Álgebra, Estadística y Probabilidad y Análisis).

- **2º Bachillerato Tecnológico:** Será la nota media ponderada de los cuatro bloques de contenidos (Álgebra (30%), Geometría(15%), Probabilidad (10%) y Análisis (40%)).

# Programación Bachillerato: Pendientes

  Para los alumnos con las matemáticas de 1º pendientes se seguirá el siguiente plan:

  Se realizarán **dos exámenes** a lo largo del curso, previa realización de los ejercicios que serán entregados a los alumnos en las fechas indicadas.

  El profesor de la asignatura y el jefe de departamento se encargarán de orientar el proceso de aprendizaje a lo largo del curso.

  **1^er^ EXAMEN**

  - En septiembre reunión con los alumnos de 2º bachillerato con el área de matemáticas pendiente. Se les entregará las  hojas de ejercicios correspondientes al primer examen, y se les darán las instrucciones para su realización.

  - El **examen se realizará a final de diciembre**.

    

  **CONTENIDOS DEL 1^er^ EXAMEN ** 

  - MATEMÁTICAS I
    - Aritmética y Álgebra
    - Trigonometría
  - MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I
    - Aritmética y Álgebra
    - Estadística

# 

  **2º EXAMEN**

  - Al finalizar el primer examen se entregarán las hojas correspondientes al segundo examen, y se darán las instrucciones para su realización.
  - El **examen se realizará en Marzo**

  **CONTENIDOS DEL 2º EXAMEN**

  - MATEMÁTICAS I
    - Números complejos
    - Estadística y Probabilidad
    - Geometría analítica
    - Funciones
  - MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I
    - Probabilidad
    - Funciones.

  La calificación final se obtendrá como media aritmética de los dos exámenes, siempre y cuando se alcance **una calificación mínima de 3** en cada uno de ellos.

  Para aquellos alumnos/as que no superen la asignatura, **se realizará un examen final** (todos los contenidos del curso) en día y fecha determinado por Jefatura de Estudios.

