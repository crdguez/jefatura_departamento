# Programación de la ESO 

## Objetivos generales de la ESO

La Educación Secundaria Obligatoria contribuirá a desarrollar en los alumnos y en las alumnas las capacidades que les permitan:

 a) Asumir responsablemente sus deberes, conocer y ejercer sus derechos en el respeto a las demás personas, practicar la **tolerancia, la cooperación y la solidaridad** entre las personas y grupos, ejercitarse en el diálogo afianzando los derechos humanos como valores comunes de una sociedad plural y prepararse para el ejercicio de la ciudadanía democrática.
 a) Desarrollar y consolidar **hábitos de disciplina, estudio y trabajo individual y en equipo** como condición necesaria para una realización eficaz de las tareas del aprendizaje y como medio de desarrollo personal.
 c) Valorar y respetar la diferencia de sexos y la igualdad de derechos y oportunidades entre ellos. Rechazar los estereotipos que supongan discriminación entre hombres y mujeres.
 d) Fortalecer sus capacidades afectivas en todos los ámbitos de la personalidad y en sus relaciones con las demás personas, así como **rechazar la violencia, los prejuicios de cualquier tipo, los comportamientos sexistas y resolver pacíficamente los conflictos**.
 e) Desarrollar destrezas básicas en la **utilización de las fuentes de información para, con sentido crítico, adquirir nuevos conocimientos**. Desarrollar las **competencias tecnológicas básicas** y avanzar en una reflexión ética sobre su funcionamiento y utilización.
 f) Concebir el **conocimiento científico como un saber integrado**, que se estructura en distintas disciplinas, así como conocer y aplicar los métodos para identificar los problemas en los diversos campos del conocimiento y de la experiencia.
 g) Desarrollar el **espíritu emprendedor y la confianza en sí mismo, la participación, el sentido crítico, la iniciativa personal y la capacidad para aprender a aprender**, planificar, tomar decisiones y asumir responsabilidades.
 h) Comprender y **expresar con corrección, oralmente y por escrito, en la lengua castellana**, textos y mensajes complejos, e iniciarse en el conocimiento, la lectura y el estudio de la literatura.
 i) Comprender y expresarse en una o más **lenguas extranjeras** de manera apropiada.
 j) Conocer, valorar y **respetar los aspectos básicos de la cultura y la historia propias y de las demás personas**, así como el patrimonio artístico y cultural.
 k) **Conocer y aceptar el funcionamiento del propio cuerpo y el de los otros y el de las otras**, respetar las diferencias, afianzar los **hábitos de cuidado y salud corporales** e incorporar la educación física y la práctica del deporte para favorecer el desarrollo personal y social. Conocer y valorar la dimensión humana de la sexualidad en toda su diversidad. Valorar críticamente los hábitos sociales relacionados con la salud, el consumo, el cuidado, la empatía y el respeto hacia los seres vivos, especialmente los animales, y el medio ambiente, contribuyendo a su conservación y mejora.
 l) **Apreciar la creación artística** y comprender el lenguaje de las distintas manifestaciones artísticas, utilizando diversos medios de expresión y representación.

## Competencias específicas y criterios de evaluación

Los criterios de evaluación son el referente para evaluar el desarrollo de las competencias específicas. Para cada una de las competencias específicas se establecen una serie de criterios de evaluación  que dependen de cada curso.

### Matemáticas de 1º a 3º ESO

#### CE.M.1. Interpretar, modelizar y resolver problemas de la vida cotidiana y propios de las matemáticas, aplicando diferentes estrategias y formas de razonamiento, para explorar distintas maneras de proceder y obtener posibles soluciones.
- 1.1 Interpretar problemas matemáticos organizando los datos dados, estableciendo las relaciones entre ellos y comprendiendo las preguntas formuladas.
- 1.2. Aplicar herramientas y estrategias apropiadas que contribuyan a la resolución de problemas.
- 1.3. Obtener soluciones matemáticas de un problema, activando los conocimientos y utilizando las herramientas tecnológicas necesarias.

#### CE.M.2. Analizar las soluciones de un problema usando diferentes técnicas y herramientas, evaluando las respuestas obtenidas, para verificar su validez e idoneidad desde un punto de vista matemático y su repercusión global.
* 2.1. Comprobar la corrección matemática de las soluciones de un problema. 
* 2.2. Comprobar la validez de las soluciones de un problema y su coherencia en el contexto planteado, evaluando el alcance y repercusión de estas desde diferentes perspectivas (de género, de sostenibilidad, de consumo responsable, etc.).

#### CE.M.3. Formular y comprobar conjeturas sencillas o plantear problemas de forma autónoma, reconociendo el valor del razonamiento y la argumentación, para generar nuevo conocimiento.
- 3.1 Formular y comprobar conjeturas sencillas de forma guiada analizando patrones, propiedades y relaciones.
- 3.2 Plantear variantes de un problema dado modificando alguno de sus datos o alguna condición del problema.
- 3.3 Emplear herramientas tecnológicas adecuadas en la investigación y comprobación de conjeturas o problemas.

#### CE.M.4. Utilizar los principios del pensamiento computacional organizando datos, descomponiendo en partes, reconociendo patrones, interpretando, modificando y creando algoritmos para modelizar situaciones y resolver problemas de forma eficaz.
- 4.1. Reconocer patrones, organizar datos y descomponer un problema en partes más simples facilitando su interpretación computacional. 
- 4.2. Modelizar situaciones y resolver problemas de forma eficaz interpretando y modificando algoritmos.

#### CE.M.5. Reconocer y utilizar conexiones entre los diferentes elementos matemáticos, interconectando conceptos y procedimientos, para desarrollar una visión de las matemáticas como un todo integrado.
- 5.1. Reconocer y usar las relaciones entre los conocimientos y experiencias matemáticas formando un todo coherente.
- 5.2. Realizar conexiones entre diferentes procesos matemáticos aplicando conocimientos y experiencias.

#### CE.M.6. Identificar las matemáticas implicadas en otras materias y en situaciones reales susceptibles de ser abordadas en términos matemáticos, interrelacionando conceptos y procedimientos, para aplicarlos en situaciones diversas.
- 6.1 Reconocer situaciones susceptibles de ser formuladas y resueltas mediante herramientas y estrategias matemáticas, estableciendo conexiones entre el mundo real y las matemáticas y usando los procesos inherentes a la investigación: inferir, medir, comunicar, clasificar y predecir.
- 6.2 Identificar conexiones coherentes entre las matemáticas y otras materias resolviendo problemas contextualizados.
- 6.3 Reconocer la aportación de las matemáticas al progreso de la humanidad y su contribución a la superación de los retos que demanda la sociedad actual.

#### CE.M.7. Representar, de forma individual y colectiva, conceptos, procedimientos, información y resultados matemáticos, usando diferentes tecnologías, para visualizar ideas y estructurar procesos matemáticos.
- 7.1 Elaborar representaciones matemáticas que ayuden en la búsqueda de estrategias de resolución de una situación problematizada.
- 7.2 Representar conceptos, procedimientos, información y resultados matemáticos de modos distintos y con diferentes herramientas, incluidas las digitales, visualizando ideas, estructurando procesos matemáticos y valorando su utilidad para compartir información.

#### CE.M.8. Comunicar de forma individual y colectiva conceptos, procedimientos y argumentos matemáticos, usando lenguaje oral, escrito o gráfico, utilizando la terminología matemática apropiada, para dar significado y coherencia a las ideas matemáticas.
- 8.1 Comunicar información utilizando el lenguaje matemático apropiado, utilizando diferentes medios, incluidos los digitales, oralmente y por escrito, al describir, explicar y justificar razonamientos, procedimientos y conclusiones.
- 8.2 Reconocer y emplear el lenguaje matemático presente en la vida cotidiana comunicando mensajes con contenido matemático con precisión y rigor.

#### CE.M.9. Desarrollar destrezas personales, identificando y gestionando emociones, poniendo en práctica estrategias de aceptación del error como parte del proceso de aprendizaje y adaptándose ante situaciones de incertidumbre, para mejorar la perseverancia en la consecución de objetivos y el disfrute en el aprendizaje de las matemáticas.
- 9.1. Gestionar las emociones propias, desarrollar el autoconcepto matemático como herramienta, generando expectativas positivas ante nuevos retos.
- 9.2. Mostrar una actitud positiva y perseverante, aceptando la crítica razonada al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.

#### CE.M.10. Desarrollar destrezas sociales reconociendo y respetando las emociones y experiencias de los demás, participando activa y reflexivamente en proyectos en equipos heterogéneos con roles asignados para construir una identidad positiva como estudiante de matemáticas, fomentar el bienestar personal y crear relaciones saludables.
- 10.1. Colaborar activamente y construir relaciones trabajando con las matemáticas -en equipos heterogéneos, respetando diferentes opiniones, comunicándose de manera efectiva, pensando de forma crítica y creativa y tomando decisiones y juicios informados.
- 10.2. Participar en el reparto de tareas que deban desarrollarse en equipo, aportando valor, favoreciendo la inclusión, la escucha activa, asumiendo el rol asignado y responsabilizándose de la propia contribución al equipo.

### Matemáticas A 4º ESO

#### CE.M.1. Interpretar, modelizar y resolver problemas de la vida cotidiana y propios de las matemáticas, aplicando diferentes estrategias y formas de razonamiento, para explorar distintas maneras de proceder y obtener posibles soluciones.
- 1.1. Reformular de forma verbal y/o gráfica, problemas matemáticos analizando los datos, las relaciones entre ellos y las preguntas planteadas.
- 1.2. Seleccionar herramientas y estrategias elaboradas valorando su eficacia e idoneidad en la resolución de problemas.
- 1.3. Obtener todas las posibles soluciones matemáticas de un problema activando los conocimientos y utilizando las herramientas tecnológicas necesarias.

#### CE.M.2. Analizar las soluciones de un problema usando diferentes técnicas y herramientas, evaluando las respuestas obtenidas, para verificar su validez e idoneidad desde un punto de vista matemático y su repercusión global.
- 2.1 Comprobar la corrección matemática de las soluciones de un problema.
- 2.2. Seleccionar las soluciones óptimas de un problema valorando tanto la corrección matemática como sus implicaciones desde diferentes perspectivas (de género, de sostenibilidad, de consumo responsable...).

#### CE.M.3. Formular y comprobar conjeturas sencillas o plantear problemas de forma autónoma, reconociendo el valor del razonamiento y la argumentación, para generar nuevo conocimiento.
- 3.1 Formular y comprobar conjeturas sencillas de forma guiada analizando patrones, propiedades y relaciones.
- 3.2 Plantear variantes de un problema dado modificando alguno de sus datos o alguna condición del problema.
- 3.3 Emplear herramientas tecnológicas adecuadas en la investigación y comprobación de conjeturas o problemas.

#### CE.M.4. Utilizar los principios del pensamiento computacional organizando datos, descomponiendo en partes, reconociendo patrones, interpretando, modificando y creando algoritmos para modelizar situaciones y resolver problemas de forma eficaz.
- 4.1. Reconocer e investigar patrones, organizar datos y descomponer un problema en partes más simples facilitando su interpretación y su tratamiento computacional.
- 4.2. Modelizar situaciones y resolver problemas de forma eficaz interpretando, modificando y creando algoritmos sencillos.

#### CE.M.5. Reconocer y utilizar conexiones entre los diferentes elementos matemáticos, interconectando conceptos y procedimientos, para desarrollar una visión de las matemáticas como un todo integrado.
- 5.1 Deducir relaciones entre los conocimientos y experiencias matemáticas, formando un todo coherente.
- 5.2 Analizar y poner en práctica conexiones entre diferentes procesos matemáticos aplicando conocimientos y experiencias previas.

#### CE.M.6. Identificar las matemáticas implicadas en otras materias y en situaciones reales susceptibles de ser abordadas en términos matemáticos, interrelacionando conceptos y procedimientos, para aplicarlos en situaciones diversas.
- 6.1 Proponer situaciones susceptibles de ser formuladas y resueltas mediante herramientas y estrategias matemáticas, estableciendo y aplicando conexiones entre el mundo real y las matemáticas, y usando los procesos inherentes a la investigación científica y matemática: inferir, medir, comunicar, clasificar y predecir.
- 6.2 Identificar y aplicar conexiones coherentes entre las matemáticas y otras materias realizando un análisis crítico.
- 6.3 Valorar la aportación de las matemáticas al progreso de la humanidad y su contribución en la superación de los retos que demanda la sociedad actual.

#### CE.M.7. Representar, de forma individual y colectiva, conceptos, procedimientos, información y resultados matemáticos, usando diferentes tecnologías, para visualizar ideas y estructurar procesos matemáticos.
- 7.1 Representar matemáticamente la información más relevante de un problema, conceptos, procedimientos y resultados matemáticos visualizando ideas y estructurando procesos matemáticos.
- 7.2 Seleccionar entre diferentes herramientas, incluidas las digitales, y formas de representación (pictórica, gráfica, verbal o simbólica) valorando su utilidad para compartir información.

#### CE.M.8. Comunicar de forma individual y colectiva conceptos, procedimientos y argumentos matemáticos, usando lenguaje oral, escrito o gráfico, utilizando la terminología matemática apropiada, para dar significado y coherencia a las ideas matemáticas.
- 8.1 Comunicar ideas, conclusiones, conjeturas y razonamientos matemáticos, utilizando diferentes medios, incluidos los digitales, con coherencia, claridad y terminología apropiada.
- 8.2 Reconocer y emplear el lenguaje matemático presente en la vida cotidiana y en diversos contextos comunicando mensajes con contenido matemático con precisión y rigor.

#### CE.M.9. Desarrollar destrezas personales, identificando y gestionando emociones, poniendo en práctica estrategias de aceptación del error como parte del proceso de aprendizaje y adaptándose ante situaciones de incertidumbre, para mejorar la perseverancia en la consecución de objetivos y el disfrute en el aprendizaje de las matemáticas.
- 9.1. Identificar y gestionar las emociones propias y desarrollar el autoconcepto matemático generando expectativas positivas ante nuevos retos.
- 9.2. Mostrar una actitud positiva y perseverante al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas aceptando la crítica razonada.

#### CE.M.10. Desarrollar destrezas sociales reconociendo y respetando las emociones y experiencias de los demás, participando activa y reflexivamente en proyectos en equipos heterogéneos con roles asignados para construir una identidad positiva como estudiante de matemáticas, fomentar el bienestar personal y crear relaciones saludables.
- 10.1. Colaborar activamente y construir relaciones trabajando con las matemáticas en equipos heterogéneos, respetando diferentes opiniones, comunicándose de manera efectiva, pensando de forma crítica y creativa, tomando decisiones y realizando juicios informados.
- 10.2. Gestionar el reparto de tareas en el trabajo en equipo, aportando valor, favoreciendo la inclusión, la escucha activa, responsabilizándose del rol asignado y de la propia contribución al equipo.

### Matemáticas B 4º ESO

#### CE.M.1. Interpretar, modelizar y resolver problemas de la vida cotidiana y propios de las matemáticas, aplicando diferentes estrategias y formas de razonamiento, para explorar distintas maneras de proceder y obtener posibles soluciones.
- 1.1. Reformular de forma verbal y gráfica problemas matemáticos, interpretando los datos, las relaciones entre ellos y las preguntas planteadas. 
- 1.2. Analizar y seleccionar diferentes herramientas y estrategias elaboradas en la resolución de un mismo problema, valorando su eficiencia. 
- 1.3. Obtener todas las soluciones matemáticas de un problema movilizando los conocimientos y utilizando las herramientas tecnológicas necesarias.

#### CE.M.2. Analizar las soluciones de un problema usando diferentes técnicas y herramientas, evaluando las respuestas obtenidas, para verificar su validez e idoneidad desde un punto de vista matemático y su repercusión global.
- 2.1 Comprobar la corrección matemática de las soluciones de un problema.
- 2.2. Justificar las soluciones óptimas de un problema desde diferentes perspectivas (matemática, de género, de sostenibilidad, de consumo responsable...).

#### CE.M.3. Formular y comprobar conjeturas sencillas o plantear problemas de forma autónoma, reconociendo el valor del razonamiento y la argumentación, para generar nuevo conocimiento.
- 3.1 Formular, comprobar e investigar conjeturas de forma guiada.
- 3.2 Plantear variantes de un problema que lleven a una generalización.
- 3.3 Emplear herramientas tecnológicas adecuadas en la investigación y comprobación de conjeturas o problemas.

#### CE.M.4. Utilizar los principios del pensamiento computacional organizando datos, descomponiendo en partes, reconociendo patrones, interpretando, modificando y creando algoritmos para modelizar situaciones y resolver problemas de forma eficaz.
- 4.1. Generalizar patrones y proporcionar una representación computacional de situaciones problematizadas.
- 4.2. Modelizar situaciones y resolver problemas de forma eficaz interpretando, modificando, generalizando y creando algoritmos.

#### CE.M.5. Reconocer y utilizar conexiones entre los diferentes elementos matemáticos, interconectando conceptos y procedimientos, para desarrollar una visión de las matemáticas como un todo integrado.
- 5.1 Deducir relaciones entre los conocimientos y experiencias matemáticas, formando un todo coherente.
- 5.2 Analizar y poner en práctica conexiones entre diferentes procesos matemáticos aplicando conocimientos y experiencias previas.

#### CE.M.6. Identificar las matemáticas implicadas en otras materias y en situaciones reales susceptibles de ser abordadas en términos matemáticos, interrelacionando conceptos y procedimientos, para aplicarlos en situaciones diversas.
- 6.1 Proponer situaciones susceptibles de ser formuladas y resueltas mediante herramientas y estrategias matemáticas, estableciendo y aplicando conexiones entre el mundo real y las matemáticas, y usando los procesos inherentes a la investigación científica y matemática: inferir, medir, comunicar, clasificar y predecir.
- 6.2 Analizar y aplicar conexiones coherentes entre las matemáticas y otras materias realizando un análisis crítico.
- 6.3 Valorar la aportación de las matemáticas al progreso de la humanidad y su contribución a la superación de los retos que demanda la sociedad actual.

#### CE.M.7. Representar, de forma individual y colectiva, conceptos, procedimientos, información y resultados matemáticos, usando diferentes tecnologías, para visualizar ideas y estructurar procesos matemáticos.
- 7.1 Representar matemáticamente la información más relevante de un problema, conceptos, procedimientos y resultados matemáticos visualizando ideas y estructurando procesos matemáticos.
- 7.2 Seleccionar entre diferentes herramientas, incluidas las digitales, y formas de representación (pictórica, gráfica, verbal o simbólica) valorando su utilidad para compartir información.

#### CE.M.8. Comunicar de forma individual y colectiva conceptos, procedimientos y argumentos matemáticos, usando lenguaje oral, escrito o gráfico, utilizando la terminología matemática apropiada, para dar significado y coherencia a las ideas matemáticas.
- 8.1 Comunicar ideas, conclusiones, conjeturas y razonamientos matemáticos, utilizando diferentes medios, incluidos los digitales, con coherencia, claridad y terminología apropiada.
- 8.2 Reconocer y emplear el lenguaje matemático presente en la vida cotidiana y en diversos contextos comunicando mensajes con contenido matemático con precisión y rigor.

#### CE.M.9. Desarrollar destrezas personales, identificando y gestionando emociones, poniendo en práctica estrategias de aceptación del error como parte del proceso de aprendizaje y adaptándose ante situaciones de incertidumbre, para mejorar la perseverancia en la consecución de objetivos y el disfrute en el aprendizaje de las matemáticas.
- 9.1. Gestionar las emociones propias, desarrollar el autoconcepto matemático generando expectativas positivas ante nuevos retos.
- 9.2. Mostrar una actitud positiva y perseverante, aceptando la crítica razonada, al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.

#### CE.M.10. Desarrollar destrezas sociales reconociendo y respetando las emociones y experiencias de los demás, participando activa y reflexivamente en proyectos en equipos heterogéneos con roles asignados para construir una identidad positiva como estudiante de matemáticas, fomentar el bienestar personal y crear relaciones saludables.
- 10.1. Colaborar activamente y construir relaciones trabajando con las matemáticas en equipos heterogéneos, respetando diferentes opiniones, comunicándose de manera efectiva, pensando de forma crítica y creativa, tomando decisiones y realizando juicios informados.
- 10.2. Gestionar el reparto de tareas en el trabajo en equipo, aportando valor, favoreciendo la inclusión, la escucha activa, responsabilizándose del rol asignado y de la propia contribución al equipo.

### Laboratorio de refuerzo de competencias clave, 1ºESO

#### CE.LRCV.1. Comprender e interpretar textos orales, escritos y multimodales, con sentido crítico, recogiendo el sentido global y la información más relevante, identificando el punto de vista y la intención del emisor y valorando su fiabilidad, su forma y su contenido, para construir conocimiento, dar respuesta a necesidades e intereses comunicativos diversos, formarse opinión y para ensanchar las posibilidades de disfrute y ocio.
- 1.1. Analizar el sentido global y la información específica y explícita de textos orales, escritos y multimodales sobre temas frecuentes y cotidianos, de relevancia personal y próximos a su experiencia, propios de los ámbitos de las relaciones interpersonales, del aprendizaje y de la ficción a través de diversos soportes.
- 1.2. Adoptar hábitos de uso crítico, seguro, y saludable de las tecnologías digitales en relación a la búsqueda e interpretación de la información.

#### CE.LRCV.2. Producir textos orales, escritos y multimodales con fluidez, coherencia, cohesión y registro adecuado, atendiendo a las convenciones propias del género discursivo elegido, y participar en interacciones orales con actitud cooperativa y respetuosa, tanto para construir conocimiento y establecer vínculos personales como para dar respuesta de manera informada, eficaz y creativa a diferentes situaciones comunicativas.
- 2.1. Planificar y producir textos breves, orales, escritos y multimodales, con coherencia, cohesión y adecuación a la situación comunicativa propuesta, siguiendo pautas establecidas, a través de herramientas analógicas y digitales, sobre asuntos cotidianos, del ámbito educativo y textos literarios.
- 2.2. Participar en interacciones orales informales de manera activa y adecuada, con actitudes de escucha activa y haciendo uso de estrategias de cooperación conversacional y cortesía lingüística.
- 2.3. Incorporar procedimientos básicos para enriquecer los textos, atendiendo a aspectos lingüísticos, con precisión léxica y corrección ortográfica y gramatical.


#### CE.LRCV.3. Resolver problemas en contextos variados, tanto matemáticos como de fuera de las matemáticas, siempre que sean cercanos y significativos, adoptando una actitud flexible a partir del uso de estrategias diversas y reflexionar sobre el propio proceso de resolución, así como construir y reconstruir conocimiento matemático a través de la resolución de dichos problemas.
- 3.1. Reformular, de forma verbal y gráfica, problemas de la vida cotidiana cercanos y significativos para el alumnado, comprendiendo las preguntas planteadas a través de diferentes estrategias o herramientas.
- 3.2. Seleccionar entre diferentes estrategias para resolver un problema justificando la estrategia seleccionada y compartiendo la reflexión que justifica la elección.
- 3.3. Comprobar la corrección matemática de las soluciones o pertinencia de las conclusiones de un problema y su coherencia en el contexto planteado.

#### CE.LRCV.4. Apreciar y reconocer el valor del razonamiento, la argumentación y la prueba, a partir de la elaboración de conjeturas y la indagación sobre ellas, de la argumentación propia y de la evaluación de argumentaciones de otros.
- 4.1 Formular conjeturas matemáticas sencillas investigando patrones, propiedades y relaciones en situaciones de aprendizaje con el andamiaje adecuado.
- 4.2. Dar ejemplos e inventar problemas sobre situaciones cercanas y significativas para el alumnado que se pueden abordar matemáticamente.
- 4.3. Argumentar la validez de conjeturas y de soluciones de un problema en términos matemáticos y en coherencia con el contexto planteado.


#### CE.LRCV.5. Utilizar el lenguaje matemático en sus diversos registros y representaciones para comunicar ideas matemáticas de forma precisa, analizar y evaluar el pensamiento matemático de otros, organizando el pensamiento matemático propio en el proceso.
- 5.1. Interpretar lenguaje matemático sencillo en situaciones cercanas y significativas para el alumnado en diferentes registros y representaciones, adquiriendo vocabulario apropiado y mostrando la comprensión del mensaje.
- 5.2. Comunicar articulando diferentes registros y formas de representación las conjeturas y procesos matemáticos utilizando lenguaje matemático adecuado.

#### CE.LRCV.6. Reconocer y emplear conexiones entre las ideas matemáticas, comprendiendo cómo estas se interconectan, así como identificar las matemáticas que aparecen en los más diversos contextos.
- 6.1. Utilizar conexiones entre diferentes elementos matemáticos movilizando conocimientos y experiencias propios.
- 6.2. Utilizar las conexiones entre las matemáticas, otras áreas y la vida cotidiana para resolver problemas en contextos no matemáticos.


### Laboratorio de refuerzo de competencias clave, 2ºESO

#### CE.LRCV.1. Comprender e interpretar textos orales, escritos y multimodales, con sentido crítico, recogiendo el sentido global y la información más relevante, identificando el punto de vista y la intención del emisor y valorando su fiabilidad, su forma y su contenido, para construir conocimiento, dar respuesta a necesidades e intereses comunicativos diversos, formarse opinión y para ensanchar las posibilidades de disfrute y ocio.
- 1.1. Extraer e interpretar el sentido global y las ideas principales, seleccionando información pertinente de textos orales, escritos y multimodales sobre temas cotidianos, del ámbito social y los medios de comunicación o literarios.
- 1.2. Adoptar hábitos de uso crítico, seguro y saludable de las tecnologías digitales en relación a la búsqueda, interpretación y la comunicación de la información.

#### CE.LRCV.2. Producir textos orales, escritos y multimodales con fluidez, coherencia, cohesión y registro adecuado, atendiendo a las convenciones propias del género discursivo elegido, y participar en interacciones orales con actitud cooperativa y respetuosa, tanto para construir conocimiento y establecer vínculos personales como para dar respuesta de manera informada, eficaz y creativa a diferentes situaciones comunicativas.
- 2.1. Planificar y producir textos orales, escritos y multimodales progresivamente más complejos, propios del ámbito social, de los medios de comunicación, así como textos literarios adecuados al nivel de madurez del alumnado.
- 2.2. Participar en interacciones orales formales de manera activa y adecuada, con actitudes de escucha activa y haciendo uso de estrategias de cooperación conversacional y cortesía lingüística.
- 2.3. Incorporar procedimientos básicos para enriquecer los textos, atendiendo a aspectos discursivos, lingüísticos y de estilo, con precisión léxica y corrección ortográfica y gramatical.

#### CE.LRCV.3. Resolver problemas en contextos variados, tanto matemáticos como de fuera de las matemáticas, siempre que sean cercanos y significativos, adoptando una actitud flexible a partir del uso de estrategias diversas y reflexionar sobre el propio proceso de resolución, así como construir y reconstruir conocimiento matemático a través de la resolución de dichos problemas.
- 3.1. Reformular, de forma verbal y gráfica, problemas de la vida cotidiana cercanos y significativos para el alumnado, comprendiendo las preguntas planteadas a través de diferentes estrategias o herramientas.
- 3.2. Seleccionar entre diferentes estrategias para resolver un problema justificando la estrategia seleccionada y compartiendo la reflexión que justifica la elección.
- 3.3. Comprobar la corrección matemática de las soluciones o pertinencia de las conclusiones de un problema y su coherencia en el contexto planteado.

#### CE.LRCV.4. Apreciar y reconocer el valor del razonamiento, la argumentación y la prueba, a partir de la elaboración de conjeturas y la indagación sobre ellas, de la argumentación propia y de la evaluación de argumentaciones de otros.
- 4.1 Formular conjeturas matemáticas sencillas investigando patrones, propiedades y relaciones en situaciones de aprendizaje con el andamiaje adecuado.
- 4.2. Dar ejemplos e inventar problemas sobre situaciones cercanas y significativas para el alumnado que se pueden abordar matemáticamente.
- 4.3. Argumentar la validez de conjeturas y de soluciones de un problema en términos matemáticos y en coherencia con el contexto planteado.

#### CE.LRCV.5. Utilizar el lenguaje matemático en sus diversos registros y representaciones para comunicar ideas matemáticas de forma precisa, analizar y evaluar el pensamiento matemático de otros, organizando el pensamiento matemático propio en el proceso.
- 5.1. Interpretar lenguaje matemático sencillo en situaciones cercanas y significativas para el alumnado en diferentes registros y representaciones, adquiriendo vocabulario apropiado y mostrando la comprensión del mensaje.
- 5.2. Comunicar articulando diferentes registros y formas de representación las conjeturas y procesos matemáticos utilizando lenguaje matemático adecuado.

#### CE.LRCV.6. Reconocer y emplear conexiones entre las ideas matemáticas, comprendiendo cómo estas se interconectan, así como identificar las matemáticas que aparecen en los más diversos contextos.
- 6.1. Utilizar conexiones entre diferentes elementos matemáticos movilizando conocimientos y experiencias propios.
- 6.2. Utilizar las conexiones entre las matemáticas, otras áreas y la vida cotidiana para resolver problemas en contextos no matemáticos.


### Matemáticas para la toma de decisiones

#### CE.MTD.1. Reconocer la importancia de la aritmética modular en un contexto tecnológico y digital, comprendiendo la necesidad y los fundamentos básicos de algoritmos de codificación sencillos y siendo capaz de aplicarlos de forma efectiva en situaciones concretas.
- 1.1. Aplicar el algoritmo de Euclides para calcular el m.c.d. de dos números y para obtener la expresión de la identidad de Bezout.
- 1.2. Resolver ecuaciones diofánticas lineales en una y dos variables, estudiando previamente la existencia de solución.
- 1.3. Poseer los fundamentos necesarios para trabajar módulo un entero m, sabiendo las diferentes propiedades que surgen según m sea primo o no.
- 1.4. Resolver de forma constructiva sistemas de congruencias lineales con una incógnita, estudiando previamente la existencia de solución.
- 1.5. Conocer y determinar unidades y divisores de cero en Z/mZ para cualquier m.
- 1.6. Aplicar el pequeño teorema de Fermat para estudiar la primalidad de un entero dado.
- 1.7. Conocer, idear y aplicar algoritmos de cifrado de sustitución y polialfabéticos sencillos, entendiendo sus vulnerabilidades.
- 1.8. Conocer los fundamentos y vulnerabilidades del algoritmo RSA, aplicándolo en casos sencillos. 


#### CE.MTD.2. Identificar la utilidad de la teoría de grafos para modelizar situaciones y problemas reales de la vida cotidiana y de materias del ámbito científico y tecnológico, empleándola para explorar distintas formas de proceder y para obtener y comunicar posibles soluciones.
- 2.1. Identificar propiedades y tipos de grafos. 
- 2.2. Clasificar grafos según distintos criterios.
- 2.3. Formular definiciones de las principales propiedades y familias de grafos haciendo uso de lenguaje especializado.
- 2.4. Proporcionar argumentos y/o contraejemplos acerca de la existencia, o no, de ciertos tipos de grafos y respecto al cumplimiento, o no, de determinadas propiedades.
- 2.5. Utilizar grafos para modelizar matemáticamente situaciones de la vida cotidiana, la ciencia y la tecnología.
- 2.6. Proponer situaciones y problemas reales susceptibles de ser modelizados utilizando la teoría de grafos.
- 2.7. Aplicar adecuadamente algoritmos sencillos sobre grafos, reflexionando sobre su eficiencia y transfiriendo el resultado a la situación real de partida.

#### CE.MTD.3. Utilizar la teoría de juegos para modelizar situaciones y problemas reales de la vida cotidiana y de materias del ámbito de las ciencias sociales y de la economía, reconociendo su aplicación a la toma de decisiones y obteniendo y expresando soluciones posibles en situaciones diversas.
- 3.1. Conocer la terminología básica propia de la teoría de juegos y utilizarla adecuadamente en situaciones oportunas. 
- 3.2. Utilizar la forma de representación apropiada para modelizar un juego o una situación determinada.
- 3.3. Comprender los conceptos de estrategia (pura y mixta) y de punto de equilibrio, así como su interpretación en situaciones concretas.
- 3.4. Resolver juegos de dos jugadores, suma cero e información perfecta mediante retropropagación.
- 3.5. Resolver completamente juegos de dos jugadores y suma cero dados en forma normal en el caso 2 × 2.
- 3.6. Expresar y comunicar los resultados de la resolución de un juego (ganancias, pérdidas, estrategias ganadores, etc.) en los términos del contexto concreto en que se está trabajando.


#### CE.MTD.4. Emplear herramientas de cálculo simbólico u otras herramientas digitales para representar resultados y procedimientos, explorar, conjeturar y comprobar propiedades, y resolver problemas, desarrollando e implementando algoritmos matemáticos sencillos.
- 4.1. Formular conjeturas acerca de propiedades de los números enteros y estudiar su posible veracidad o falsedad de forma computacional.
- 4.2. Utilizar herramientas informáticas para explorar propiedades de grafos.
- 4.3. Diseñar algoritmos propios para resolver problemas aritméticos en Z y en Z/mZ.
- 4.4. Expresar en pseudocódigo los algoritmos aritméticos sencillos diseñados.
- 4.5. Analizar y comprender el funcionamiento de algoritmos sencillos expresados en pseudocódigo en contextos de aritmética, teoría de grafos y teoría de juegos.



## Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas





