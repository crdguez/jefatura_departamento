# Programación de Bachillerato

## Objetivos generales del Bachillerato

El Bachillerato contribuirá a desarrollar en los alumnos y las alumnas las capacidades que les permitan:

 a) Ejercer la ciudadanía democrática, desde una perspectiva global, y adquirir una **conciencia cívica responsable**, inspirada por los valores de la Constitución Española, así como por los derechos humanos, que fomente la corresponsabilidad en la construcción de una sociedad justa y equitativa.
 b) Consolidar una **madurez personal, afectivo-sexual y social** que les permita actuar de forma respetuosa, responsable y autónoma y desarrollar su espíritu crítico. Prever, detectar y resolver pacíficamente los conflictos personales, familiares y sociales, así como las posibles situaciones de violencia.
 c) Fomentar la **igualdad efectiva de derechos y oportunidades de mujeres y hombres**, analizar y valorar críticamente las desigualdades existentes, así como el reconocimiento y enseñanza del papel de las mujeres en la historia e impulsar la igualdad real y la no discriminación por razón de nacimiento, sexo, origen racial o étnico, discapacidad, edad, enfermedad, religión o creencias, orientación sexual o identidad de género o cualquier otra condición o circunstancia personal o social.
 d) Afianzar los **hábitos de lectura, estudio y disciplina**, como condiciones necesarias para el eficaz aprovechamiento del aprendizaje, y como medio de desarrollo personal.
 e) **Dominar, tanto en su expresión oral como escrita, la lengua castellana** y, en su caso, la lengua cooficial de su comunidad autónoma.
 f) Expresarse con fluidez y corrección en una o más **lenguas extranjeras**.
 g) Utilizar con solvencia y responsabilidad las **tecnologías de la información y la comunicación**.
 h) Conocer y **valorar críticamente** las realidades del mundo contemporáneo, sus antecedentes históricos y los principales factores de su evolución. 
Participar de forma solidaria en el desarrollo y mejora de su entorno social.
 i) Acceder a los **conocimientos científicos y tecnológicos** fundamentales y dominar las habilidades básicas propias de la modalidad elegida.
 j) Comprender los elementos y procedimientos fundamentales de la investigación y de los métodos científicos. 
Conocer y valorar de forma crítica la **contribución de la ciencia y la tecnología** en el cambio de las condiciones de vida, así como afianzar la sensibilidad y el respeto hacia el medio ambiente
 k) Afianzar el **espíritu emprendedor** con actitudes de creatividad, flexibilidad, iniciativa, trabajo en equipo, confianza en uno mismo y sentido crítico.
 l) Desarrollar la **sensibilidad artística y literaria**, así como el criterio estético, como fuentes de formación y enriquecimiento cultural.
 m) Utilizar la educación física y el deporte para favorecer el desarrollo personal y social. Afianzar los hábitos de actividades físico-deportivas para favorecer el **bienestar físico y mental**, así como medio de desarrollo personal y social.
 n) Afianzar actitudes de respeto y prevención en el ámbito de la **movilidad segura y saludable**
 n) Fomentar una actitud responsable y comprometida en la lucha contra el **cambio climático** y en la defensa del desarrollo sostenible.


## Competencias específicas y criterios de evaluación

Los criterios de evaluación son el referente para evaluar el desarrollo de las competencias específicas. Para cada una de las competencias específicas se establecen una serie de criterios de evaluación  que dependen de cada curso.

### Matemáticas I

#### CE.M.1. Modelizar y resolver problemas de la vida cotidiana y de la ciencia y la tecnología aplicando diferentes estrategias y formas de razonamiento para obtener posibles soluciones.
- 1.1. Manejar algunas estrategias y herramientas, incluidas las digitales, en la modelización y resolución de problemas de la vida cotidiana y de la ciencia y la tecnología, evaluando su eficiencia en cada caso.
- 1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado.

#### CE.M.2. Verificar la validez de las posibles soluciones de un problema empleando el razonamiento y la argumentación para contrastar su idoneidad.
- 2.1 Comprobar la validez matemática de las posibles soluciones de un problema, utilizando el razonamiento y la argumentación.
- 2.2 Seleccionar la solución más adecuada de un problema en función del contexto (de sostenibilidad, de consumo responsable, equidad), usando el razonamiento y la argumentación.

#### CE.M.3. Formular o investigar conjeturas o problemas, utilizando el razonamiento y la argumentación, con apoyo de herramientas tecnológicas, para generar nuevo conocimiento matemático.
- 3.1. Adquirir nuevo conocimiento matemático a partir de la formulación de conjeturas y problemas de forma guiada.
- 3.2. Emplear herramientas tecnológicas adecuadas en la formulación o investigación de conjeturas o problemas.

#### CE.M.4. Utilizar el pensamiento computacional de forma eficaz, modificando, creando y generalizando algoritmos, para modelizar y resolver situaciones de la vida cotidiana y del ámbito de la Ciencia y la Tecnología.
- 4.1. Interpretar y resolver situaciones problematizadas de la vida cotidiana y de la ciencia y la tecnología, utilizando el pensamiento computacional, modificando y creando algoritmos.

#### CE.M.5. Establecer, investigar y utilizar conexiones entre las diferentes ideas matemáticas estableciendo vínculos entre conceptos, procedimientos, argumentos y modelos para dar significado y estructurar el aprendizaje matemático.
- 5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas.
- 5.2. Resolver problemas en contextos matemáticos estableciendo y aplicando conexiones entre las diferentes ideas matemáticas.

#### CE.M.6. Descubrir los vínculos de las matemáticas con otras materias y profundizar en sus conexiones, interrelacionando conceptos y procedimientos, para modelizar, resolver problemas y desarrollar la capacidad crítica, creativa e innovadora en situaciones diversas.
- 6.1 Resolver problemas en situaciones diversas, utilizando procesos matemáticos, estableciendo y aplicando conexiones entre el mundo real, otras materias y las matemáticas.
- 6.2 Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad.

#### CE.M.7. Representar conceptos, procedimientos e información matemáticos seleccionando diferentes tecnologías, para visualizar ideas y estructurar razonamientos matemáticos.
- 7.1. Representar ideas matemáticas estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas para la resolución de problemas.
- 7.2. Seleccionar y utilizar diversas formas de representación valorando su utilidad para compartir información.

#### CE.M.8. Comunicar las ideas matemáticas, de forma individual y colectiva, empleando el soporte, la terminología y el rigor apropiados, para organizar y consolidar el pensamiento matemático.
- 8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados.
- 8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor.

#### CE.M.9. Utilizar destrezas personales y sociales, identificando y gestionando las propias emociones, respetando las de los demás y organizando activamente el trabajo en equipos heterogéneos, aprendiendo del error como parte del proceso de aprendizaje y afrontando situaciones de incertidumbre, para perseverar en la consecución de objetivos en el aprendizaje de las matemáticas.
- 9.1 Afrontar las situaciones de incertidumbre identificando y gestionando emociones y aceptando y aprendiendo del error como parte del proceso de aprendizaje de las matemáticas.
- 9.2 Mostrar una actitud positiva y perseverante, aceptando y aprendiendo de la crítica razonada al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.
- 9.3 Participar en tareas matemáticas de forma activa en equipos heterogéneos, respetando las emociones y experiencias de los demás, escuchando su razonamiento, identificando las habilidades sociales más propicias y fomentando el bienestar grupal y las relaciones saludables.

### Matemáticas II

#### CE.M.1. Modelizar y resolver problemas de la vida cotidiana y de la ciencia y la tecnología aplicando diferentes estrategias y formas de razonamiento para obtener posibles soluciones.
- 1.1. Manejar diferentes estrategias y herramientas, incluidas las digitales, que modelizan y resuelven problemas de la vida cotidiana y de la ciencia y la tecnología, seleccionando las más adecuadas según su eficiencia.
- 1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de la ciencia y la tecnología, describiendo el procedimiento utilizado.

#### CE.M.2. Verificar la validez de las posibles soluciones de un problema empleando el razonamiento y la argumentación para contrastar su idoneidad.
- 2.1. Demostrar la validez matemática de las posibles soluciones de un problema utilizando el razonamiento y la argumentación.
- 2.2. Seleccionar la solución más adecuada de un problema en función del contexto (de sostenibilidad, de consumo responsable, equidad...) usando el razonamiento y la argumentación.

#### CE.M.3. Formular o investigar conjeturas o problemas, utilizando el razonamiento y la argumentación, con apoyo de herramientas tecnológicas, para generar nuevo conocimiento matemático.
- 3.1. Adquirir nuevo conocimiento matemático mediante la formulación, razonamiento y justificación de conjeturas y problemas de forma autónoma.
- 3.2. Integrar el uso de herramientas tecnológicas en la formulación o investigación de conjeturas y problemas.

#### CE.M.4. Utilizar el pensamiento computacional de forma eficaz, modificando, creando y generalizando algoritmos, para modelizar y resolver situaciones de la vida cotidiana y del ámbito de la Ciencia y la Tecnología.
- 4.1. Interpretar, modelizar y resolver situaciones problematizadas de la vida cotidiana y de la ciencia y la tecnología, utilizando el pensamiento computacional, modificando, creando y generalizando algoritmos.

#### CE.M.5. Establecer, investigar y utilizar conexiones entre las diferentes ideas matemáticas estableciendo vínculos entre conceptos, procedimientos, argumentos y modelos para dar significado y estructurar el aprendizaje matemático.
- 5.1. Demostrar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas.
- 5.2. Resolver problemas en contextos matemáticos estableciendo y aplicando conexiones entre las diferentes ideas matemáticas.

#### CE.M.6. Descubrir los vínculos de las matemáticas con otras materias y profundizar en sus conexiones, interrelacionando conceptos y procedimientos, para modelizar, resolver problemas y desarrollar la capacidad crítica, creativa e innovadora en situaciones diversas.
- 6.1 Resolver problemas en situaciones diversas, utilizando procesos matemáticos, estableciendo y aplicando conexiones entre el mundo real, otras materias y las matemáticas.
- 6.2 Analizar la aportación de las matemáticas al progreso de la humanidad, reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos científicos y tecnológicos que se plantean en la sociedad.

#### CE.M.7. Representar conceptos, procedimientos e información matemáticos seleccionando diferentes tecnologías, para visualizar ideas y estructurar razonamientos matemáticos.
- 7.1. Representar ideas matemáticas estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas para la resolución de problemas.
- 7.2. Seleccionar y utilizar diversas formas de representación valorando su utilidad para compartir información.

#### CE.M.8. Comunicar las ideas matemáticas, de forma individual y colectiva, empleando el soporte, la terminología y el rigor apropiados, para organizar y consolidar el pensamiento matemático.
- 8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados.
- 8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor.

#### CE.M.9. Utilizar destrezas personales y sociales, identificando y gestionando las propias emociones, respetando las de los demás y organizando activamente el trabajo en equipos heterogéneos, aprendiendo del error como parte del proceso de aprendizaje y afrontando situaciones de incertidumbre, para perseverar en la consecución de objetivos en el aprendizaje de las matemáticas.
- 9.1 Afrontar las situaciones de incertidumbre y tomar decisiones evaluando distintas opciones, identificando y gestionando emociones, y aceptando y aprendiendo del error como parte del proceso de aprendizaje de las matemáticas.
- 9.2 Mostrar una actitud positiva y perseverante, aceptando y aprendiendo de la crítica razonada al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.
- 9.3 Trabajar en tareas matemáticas de forma activa en equipos heterogéneos, respetando las emociones y experiencias de los demás, escuchando su razonamiento, aplicando las habilidades sociales más propicias y fomentando el bienestar del equipo y las relaciones saludables.

### Matemáticas aplicadas a las Ciencias Sociales I

#### CE.MCS.1. Modelizar y resolver problemas de la vida cotidiana y de las Ciencias Sociales aplicando diferentes estrategias y formas de razonamiento para obtener posibles soluciones.
- 1.1. Emplear algunas estrategias y herramientas, incluidas las digitales, para resolver problemas de la vida cotidiana y de las Ciencias Sociales, valorando su eficiencia en cada caso.
- 1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de las ciencias sociales, describiendo el procedimiento realizado.

#### CE.MCS.2. Verificar la validez de las posibles soluciones de un problema empleando el razonamiento y la argumentación para contrastar su idoneidad.
- 2.1. Comprobar la validez matemática de las posibles soluciones de un problema utilizando el razonamiento y la argumentación.
- 2.2. Seleccionar la solución más adecuada de un problema en función del contexto (de sostenibilidad, de consumo responsable, equidad...) usando el razonamiento y la argumentación.

#### CE.MCS.3. Formular o investigar conjeturas o problemas, utilizando el razonamiento y la argumentación, con apoyo de herramientas tecnológicas, para generar nuevo conocimiento matemático.
- 3.1. Adquirir nuevo conocimiento matemático mediante la formulación de conjeturas y problemas de forma guiada.
- 3.2. Emplear herramientas tecnológicas adecuadas en la formulación o investigación de conjeturas o problemas.

#### CE.MCS.4. Utilizar el pensamiento computacional de forma eficaz, modificando, creando y generalizando algoritmos que resuelvan problemas mediante el uso de las matemáticas, para modelizar y resolver situaciones de la vida cotidiana y del ámbito de las ciencias sociales.
- 4.1. Interpretar, modelizar y resolver situaciones problematizadas de la vida cotidiana y de las Ciencias Sociales, utilizando el pensamiento computacional, modificando o creando algoritmos.

#### CE.MCS.5. Establecer, investigar y utilizar conexiones entre las diferentes ideas matemáticas estableciendo vínculos entre conceptos, procedimientos, argumentos y modelos para dar significado y estructurar el aprendizaje matemático.
- 5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas.
- 5.2. Resolver problemas estableciendo y aplicando conexiones entre las diferentes ideas matemáticas.

#### CE.MCS.6. Descubrir los vínculos de las matemáticas con otras materias y profundizar en sus conexiones, interrelacionando conceptos y procedimientos, para modelizar, resolver problemas y desarrollar la capacidad crítica, creativa e innovadora en situaciones diversas.
- 6.1. Resolver problemas en situaciones diversas utilizando procesos matemáticos, estableciendo y aplicando conexiones entre el mundo real, otras materias y las Matemáticas.
- 6.2. Analizar la aportación de las Matemáticas al progreso de la humanidad reflexionando sobre su contribución en la propuesta de soluciones a situaciones complejas y a los retos en las Ciencias Sociales que se plantean.

#### CE.MCS.7. Representar conceptos, procedimientos e información matemáticos seleccionando diferentes tecnologías, para visualizar ideas y estructurar razonamientos matemáticos.
- 7.1 Representar ideas matemáticas, estructurando diferentes razonamientos matemáticos y seleccionando las tecnologías más adecuadas para la resolución de problemas.
- 7.2 Seleccionar y utilizar diversas formas de representación, valorando su utilidad para compartir información.

#### CE.MCS.8. Comunicar las ideas matemáticas, de forma individual y colectiva, empleando el soporte, la terminología y el rigor apropiados, para organizar y consolidar el pensamiento matemático.
- 8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados.
- 8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor.

#### CE.MCS.9. Utilizar destrezas personales y sociales, identificando y gestionando las propias emociones, respetando las de los demás y organizando activamente el trabajo en equipos heterogéneos, aprendiendo del error como parte del proceso de aprendizaje y afrontando situaciones de incertidumbre, para perseverar en la consecución de objetivos en el aprendizaje de las matemáticas.
- 9.1. Afrontar las situaciones de incertidumbre, identificando y gestionando emociones y aceptando y aprendiendo del error como parte del proceso de aprendizaje de las matemáticas.
- 9.2. Mostrar una actitud positiva y perseverante, aceptando y aprendiendo de la crítica razonada al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.
- 9.3. Participar en tareas matemáticas de forma activa en equipos heterogéneos, respetando las emociones y experiencias de los demás, escuchando su razonamiento, identificando las habilidades sociales más propicias y fomentando el bienestar grupal y las relaciones saludables.


### Matemáticas aplicadas a las Ciencias Sociales II 

#### CE.MCS.1. Modelizar y resolver problemas de la vida cotidiana y de las Ciencias Sociales aplicando diferentes estrategias y formas de razonamiento para obtener posibles soluciones.
- 1.1. Emplear diferentes estrategias y herramientas, incluidas las digitales que resuelvan problemas de la vida cotidiana y de las Ciencias Sociales, seleccionando la más adecuada según su eficiencia.
- 1.2. Obtener todas las posibles soluciones matemáticas de problemas de la vida cotidiana y de las Ciencias Sociales, describiendo el procedimiento realizado.


#### CE.MCS.2. Verificar la validez de las posibles soluciones de un problema empleando el razonamiento y la argumentación para contrastar su idoneidad.
- 2.1. Demostrar la validez matemática de las posibles soluciones de un problema utilizando el razonamiento y la argumentación.
- 2.2. Seleccionar la solución más adecuada de un problema en función del contexto (de sostenibilidad, de consumo responsable, equidad...) usando el razonamiento y la argumentación.


#### CE.MCS.3. Formular o investigar conjeturas o problemas, utilizando el razonamiento y la argumentación, con apoyo de herramientas tecnológicas, para generar nuevo conocimiento matemático.
- 3.1. Adquirir nuevo conocimiento matemático mediante la formulación, razonamiento y justificación de conjeturas y problemas de forma autónoma.
- 3.2. Integrar el uso de herramientas tecnológicas en la formulación o investigación de conjeturas y problemas.


#### CE.MCS.4. Utilizar el pensamiento computacional de forma eficaz, modificando, creando y generalizando algoritmos que resuelvan problemas mediante el uso de las matemáticas, para modelizar y resolver situaciones de la vida cotidiana y del ámbito de las ciencias sociales.
- 4.1. Interpretar, modelizar y resolver situaciones problematizadas de la vida cotidiana y las Ciencias Sociales utilizando el pensamiento computacional, modificando, creando y generalizando algoritmos.

#### CE.MCS.5. Establecer, investigar y utilizar conexiones entre las diferentes ideas matemáticas estableciendo vínculos entre conceptos, procedimientos, argumentos y modelos para dar significado y estructurar el aprendizaje matemático.
- 5.1. Manifestar una visión matemática integrada, investigando y conectando las diferentes ideas matemáticas.
- 5.2. Resolver problemas estableciendo y aplicando conexiones entre las diferentes ideas matemáticas


#### CE.MCS.6. Descubrir los vínculos de las matemáticas con otras materias y profundizar en sus conexiones, interrelacionando conceptos y procedimientos, para modelizar, resolver problemas y desarrollar la capacidad crítica, creativa e innovadora en situaciones diversas.
- 6.1. Resolver problemas en situaciones diversas utilizando procesos matemáticos, reflexionando, estableciendo y aplicando conexiones entre el mundo real, otras materias y las Matemáticas.
- 6.2. Analizar la aportación de las Matemáticas al progreso de la humanidad valorando su contribución en la propuesta de soluciones a situaciones complejas y a los retos que se plantean en las Ciencias Sociales.


#### CE.MCS.7. Representar conceptos, procedimientos e información matemáticos seleccionando diferentes tecnologías, para visualizar ideas y estructurar razonamientos matemáticos.
- 7.1. Representar y visualizar ideas matemáticas estructurando diferentes procesos matemáticos y seleccionando las tecnologías más adecuadas para la resolución de problemas.
- 7.2. Seleccionar y utilizar diversas formas de representación valorando su utilidad para compartir información.


#### CE.MCS.8. Comunicar las ideas matemáticas, de forma individual y colectiva, empleando el soporte, la terminología y el rigor apropiados, para organizar y consolidar el pensamiento matemático.
- 8.1. Mostrar organización al comunicar las ideas matemáticas empleando el soporte, la terminología y el rigor apropiados.
- 8.2. Reconocer y emplear el lenguaje matemático en diferentes contextos, comunicando la información con precisión y rigor.


#### CE.MCS.9. Utilizar destrezas personales y sociales, identificando y gestionando las propias emociones, respetando las de los demás y organizando activamente el trabajo en equipos heterogéneos, aprendiendo del error como parte del proceso de aprendizaje y afrontando situaciones de incertidumbre, para perseverar en la consecución de objetivos en el aprendizaje de las matemáticas.
- 9.1. Afrontar las situaciones de incertidumbre y tomar decisiones evaluando distintas opciones, identificando y gestionando emociones y aceptando y aprendiendo del error como parte del proceso de aprendizaje de las matemáticas.
- 9.2. Mostrar perseverancia y una motivación positiva, aceptando y aprendiendo de la crítica razonada al hacer frente a las diferentes situaciones de aprendizaje de las matemáticas.
- 9.3. Trabajar en tareas matemáticas de forma activa en equipos heterogéneos, respetando las emociones y experiencias de los demás, escuchando su razonamiento, aplicando las habilidades sociales más propicias y fomentando el bienestar del equipo y las relaciones saludables.





## Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas
