# Introducción

En este documento se presenta la programación del departamento de Matemáticas del Instituto de enseñanza secundaria Pedro Cerrada situado en la localidad de Utebo. 
La programación didáctica es el documento en el que se desarrollan los currículos de la ESO y de Bachillerato tomando como referencia los Proyectos Curriculares de Etapa correspondientes. 

## Elementos de currículo

Los elementos del currículo que establece la LOMLOE y sobre los cuales se desarrolla la presente programación son: 

a) **Objetivos:** logros que se espera que el alumnado haya alcanzado al finalizar la etapa y cuya consecución está vinculada a la adquisición de las competencias clave y de las competencias específicas de las diferentes materias o ámbitos. 

b) **Competencias clave:** desempeños que se consideran imprescindibles para que el alumnado pueda progresar con garantías de éxito en su itinerario formativo, y afrontar los principales retos y desafíos globales y locales. Dichas competencias clave aparecen recogidas en el anexo I de esta Orden, definiendo el Perfil de salida del alumnado al término de la enseñanza básica, y son la adaptación al sistema educativo español de las competencias clave establecidas en la Recomendación del Consejo de la Unión Europea de 22 de mayo de 2018 relativa a las competencias clave para el aprendizaje permanente. 

c) **Competencias específicas:** desempeños que el alumnado debe poder desplegar en actividades o en situaciones cuyo abordaje requiere de los saberes básicos de cada materia o ámbito. Las competencias específicas constituyen un elemento de conexión entre, por una parte, el Perfil de salida del alumnado, y por otra, los saberes básicos de las materias o ámbitos y los criterios de evaluación. 

d) **Criterios de evaluación:** referentes que indican los niveles de desempeño esperados en el alumnado en las actividades o situaciones de aprendizaje a las que se refieren las competencias específicas de cada materia o ámbito en un momento determinado de su proceso de aprendizaje. 

e) **Saberes básicos:** conocimientos, destrezas y actitudes que constituyen los contenidos propios de una materia o ámbito cuyo aprendizaje es necesario para la adquisición de las competencias específicas. 

f) **Situaciones de aprendizaje**: situaciones y actividades que implican el despliegue por parte del alumnado de actuaciones que contribuyen al desarrollo y adquisición de las competencias clave y las competencias específicas y cuyo diseño involucra el aprendizaje de diferentes saberes básicos asociados a una o varias materias o ámbitos.

## Normativa

Para la realización de la programación se ha seguido lo establecido en la siguiente normativa de carácter estatal y autonómico: 

- [Ley Orgánica 2/2006, de 3 de mayo, de Educación](https://www.boe.es/buscar/act.php?id=BOE-A-2006-7899)
-  [Orden ECD/1172/2022, de 2 de agosto, por la que se aprueban el currículo y las características de la evaluación 
de la Educación Secundaria Obligatoria ](https://educa.aragon.es/documents/20126/2789389/ECD+1172+2022+de+2+de+agosto+%28curr%C3%ADculo+y+evaluaci%C3%B3n+ESO%29.pdf/8659291a-b7d9-66a8-b6d6-59d9c88d78b1?t=1661768667394)
- [Orden ECD/1173/2022, de 3 de agosto, por la que se aprueban el currículo y las características de la evaluación del Bachillerato](https://educa.aragon.es/documents/20126/2789392/ECD+1173+2022+de+3+de+agosto+%28curr%C3%ADculo+y+evaluaci%C3%B3n+Bachillerato%29.pdf/df988f84-b700-ada0-4a95-98e060083e30?t=1661776021789)
-  [ORDEN ECD/2146/2018, de 28 de diciembre, para el desarrollo del proceso de reflexión en torno a las tareas escolares en el proceso de aprendizaje](https://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=1056142882525)

Desde la página web del departamento de educación del Gobierno de Aragón se puede acceder a toda la normativa de manera organizada:

- Normativa ESO: [https://educa.aragon.es/web/guest/-/normativa-eso](https://educa.aragon.es/web/guest/-/normativa-eso)
- Normativa Bachillerato: [https://educa.aragon.es/web/guest/-/norma-bachillerato](https://educa.aragon.es/web/guest/-/norma-bachillerato)

<!---

## Contenidos de la programación de la ESO

Artículo 59  de la Orden ECD/1172/2022, de 2 de agosto, por la que se aprueban el currículo y las características de la evaluación 
de la Educación Secundaria Obligatoria:

*Programaciones didácticas. 1. Los departamentos, equipos didácticos o el órgano de coordinación didáctica que corresponda, tomando como referencia el Proyecto Curricular de Etapa, desarrollarán el currículo establecido en esta Orden mediante la programación didáctica del curso. Se considerarán los principios pedagógicos y metodológicos, establecidos los artículos 4 y 10 respectivamente de esta Orden y la contribución al desarrollo de las competencias clave. 2. La programación didáctica deberá ser el instrumento de planificación curricular específico y necesario para desarrollar el proceso de enseñanza y aprendizaje del alumnado de manera coordinada entre todo el profesorado que integra el departamento, equipo didáctico o el órgano de coordinación didáctica que corresponda. 3. Las programaciones didácticas de cada curso incluirán, al menos, los siguientes aspectos en cada materia o ámbito: a) Competencias específicas y criterios de evaluación asociados a ellas. b) Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas. c) Procedimientos e instrumentos de evaluación, con especial atención al carácter forma tivo de la evaluación y a su vinculación con los criterios de evaluación. d) Criterios de calificación. e) Características de la evaluación inicial, criterios para su valoración, así como conse cuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación. f) Actuaciones generales de atención a las diferencias individuales y adaptaciones curri culares para el alumnado que las precise. g) Plan de seguimiento personal para el alumnado que no promociona, de acuerdo con lo establecido en al artículo 19.4 de esta Orden. h) Plan de refuerzo personalizado para materias o ámbitos no superados, de acuerdo con lo establecido en al artículo 20 de esta Orden. i) Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfo ques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios. j) Concreción del Plan Lector establecido en el Proyecto Curricular de Etapa. k) Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa. l) Concreción del Plan de utilización de las tecnologías digitales establecido en el Pro yecto Curricular de Etapa. m) En su caso, medidas complementarias que se plantean para el tratamiento de las materias o ámbitos dentro de proyectos o itinerarios bilingües o plurilingües, o de proyectos de lenguas y modalidades lingüísticas propias de la Comunidad Autónoma de Aragón. n) Mecanismos de revisión, evaluación y modificación de las programaciones didácticas en relación con los resultados académicos y procesos de mejora. ñ) Actividades complementarias y extraescolares programadas por cada departamento, equipos didáctico u órgano de coordinación didáctica que corresponda, de acuerdo con el programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.*

Por tanto, la programación contará al menos con los siguientes apartados:

- Competencias específicas y criterios de evaluación asociados a ellas. 

- Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas. 

- Procedimientos e instrumentos de evaluación, con especial atención al carácter forma tivo de la evaluación y a su vinculación con los criterios de evaluación. 

- Criterios de calificación.

- Características de la evaluación inicial, criterios para su valoración, así como consecuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación. 

- Actuaciones generales de atención a las diferencias individuales y adaptaciones curri culares para el alumnado que las precise. 

- Plan de seguimiento personal para el alumnado que no promociona, de acuerdo con lo establecido en al artículo 19.4 de esta Orden. 

- Plan de refuerzo personalizado para materias o ámbitos no superados, de acuerdo con lo establecido en al artículo 20 de esta Orden. 

- Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfo ques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios.

- Concreción del Plan Lector establecido en el Proyecto Curricular de Etapa. 

- Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa. 

- Concreción del Plan de utilización de las tecnologías digitales establecido en el Pro yecto Curricular de Etapa. 

- En su caso, medidas complementarias que se plantean para el tratamiento de las materias o ámbitos dentro de proyectos o itinerarios bilingües o plurilingües, o de proyectos de lenguas y modalidades lingüísticas propias de la Comunidad Autónoma de Aragón. 

- Mecanismos de revisión, evaluación y modificación de las programaciones didácticas en relación con los resultados académicos y procesos de mejora. 

- Actividades complementarias y extraescolares programadas por cada departamento, equipos didáctico u órgano de coordinación didáctica que corresponda, de acuerdo con el programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.


## Contenidos de la programación de Bachillerato

Artículo 54 de la Orden ECD/1173/2022, de 3 de agosto, por la que se aprueban el currículo y las características de la evaluación del Bachillerato: 

*Programaciones didácticas. 1. Los departamentos, equipos o el órgano de coordinación didáctica que corresponda, tomando como referencia el Proyecto Curricular de Etapa, desarrollarán el currículo establecido en esta Orden mediante la programación didáctica del curso. Se considerarán los principios pedagógicos y metodológicos, establecidos en los artículos 4 y 5 de esta Orden, y la contribución al desarrollo de las competencias clave. 2. La programación didáctica deberá ser el instrumento de planificación curricular específico y necesario para desarrollar el proceso de enseñanza y aprendizaje del alumnado de manera coordinada entre todo el profesorado que integra el departamento, equipo u órgano de coordinación que corresponda. 3. Las programaciones didácticas de cada curso incluirán, al menos, los siguientes aspectos en cada materia: a) Competencias específicas y criterios de evaluación asociados a ellas. b) Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas. c) Procedimientos e instrumentos de evaluación, con especial atención al carácter forma tivo de la evaluación y a su vinculación con los criterios de evaluación. d) Criterios de calificación. e) Características de la evaluación inicial, criterios para su valoración, así como conse cuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación. f) Actuaciones generales de atención a las diferencias individuales. g) Plan de recuperación de materias pendientes. h) Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfo ques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios. i) Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa. j) Concreción del Plan de utilización de las Tecnologías digitales establecido en el Pro yecto Curricular de Etapa. k) En su caso, medidas complementarias que se plantean para el tratamiento de las ma terias dentro de proyectos o itinerarios bilingües o plurilingües o de proyectos de lenguas y modalidades lingüísticas propias de la comunidad autónoma de Aragón. l) Mecanismos de revisión, evaluación y modificación de las programaciones Didácticas en relación con los resultados académicos y procesos de mejora. m) Actividades complementarias y extraescolares programadas por cada departamento, equipo u órgano de coordinación didáctica que corresponda, de acuerdo con el Programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.*

Por tanto, la programación contará al menos con los siguientes apartados:

-  Competencias específicas y criterios de evaluación asociados a ellas

-  Concreción, agrupamiento y secuenciación de los saberes básicos y de los criterios de evaluación en unidades didácticas. 

-  Procedimientos e instrumentos de evaluación, con especial atención al carácter forma tivo de la evaluación y a su vinculación con los criterios de evaluación

-  Criterios de calificación

-  Características de la evaluación inicial, criterios para su valoración, así como conse cuencias de sus resultados en la programación didáctica y, en su caso, el diseño de los instrumentos de evaluación

-  Actuaciones generales de atención a las diferencias individuales

-  Plan de recuperación de materias pendientes

-  Estrategias didácticas y metodológicas: Organización, recursos, agrupamientos, enfo ques de enseñanza, criterios para la elaboración de situaciones de aprendizaje y otros elementos que se consideren necesarios

-  Concreción del Plan de implementación de elementos transversales establecido en el Proyecto Curricular de Etapa

-  Concreción del Plan de utilización de las Tecnologías digitales establecido en el Proyecto Curricular de Etapa

-  En su caso, medidas complementarias que se plantean para el tratamiento de las ma terias dentro de proyectos o itinerarios bilingües o plurilingües o de proyectos de lenguas y modalidades lingüísticas propias de la comunidad autónoma de Aragón

-  Mecanismos de revisión, evaluación y modificación de las programaciones Didácticas en relación con los resultados académicos y procesos de mejora

-  Actividades complementarias y extraescolares programadas por cada departamento, equipo u órgano de coordinación didáctica que corresponda, de acuerdo con el Programa anual de actividades complementarias y extraescolares establecidas por el centro, concretando la incidencia de las mismas en la evaluación del alumnado.

-->






